import {
  GET_APPLICATION_HIT,
  PROGRAMS_ERROR,
  CLEAR_PROGRAMS,
  GET_APPLICATIONS_LAST_LIMIT,
  GET_APPLICATION_CATEGORIES,
  GET_APPLICATIONS,
  GET_APPLICATION_BY_ID,
  GET_APPLICATIONS_BY_CATEGORY,
  GET_APPLICATIONS_RANDOM,
  GET_APPLICATIONS_RANDOM_BY_CATEGORY
} from "../actions/types";

const initialState = {
  application: {},
  applications: [],
  applications_hit: [],
  applications_limit: [],
  app_categories: [],
  applications_category: [],
  applications_random: [],
  applications_random_category: [],
  loading: true,
  error: {}
};

export default function(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_APPLICATION_HIT:
      return {
        ...state,
        applications_hit: payload,
        loading: false
      };
    case GET_APPLICATIONS_LAST_LIMIT:
      return {
        ...state,
        applications_limit: payload,
        loading: false
      };
    case GET_APPLICATION_CATEGORIES:
      return {
        ...state,
        app_categories: payload,
        loading: false
      };
    case GET_APPLICATIONS:
      return {
        ...state,
        applications: payload,
        loading: false
      };
    case GET_APPLICATION_BY_ID:
      return {
        ...state,
        application: payload,
        loading: false
      };
    case GET_APPLICATIONS_BY_CATEGORY:
      return {
        ...state,
        applications_category: payload,
        loading: false
      };
    case GET_APPLICATIONS_RANDOM:
      return {
        ...state,
        applications_random: payload,
        loading: false
      };
    case GET_APPLICATIONS_RANDOM_BY_CATEGORY:
      return {
        ...state,
        applications_random_category: payload,
        loading: false
      };
    case PROGRAMS_ERROR:
      return {
        ...state,
        error: payload,
        loading: false
      };

    case CLEAR_PROGRAMS:
      return {
        ...state,
        application: {},
        applications: [],
        applications_limit: [],
        applications_hit: [],
        app_categories: [],
        applications_category: [],
        applications_random: [],
        applications_random_category: [],
        loading: true
      };

    default:
      return state;
  }
}
