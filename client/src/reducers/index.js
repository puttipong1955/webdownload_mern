import { combineReducers } from "redux";
import program from "./program";
import application from "./application";
import game from "./game";
import auth from "./auth";

export default combineReducers({
  program,
  application,
  game,
  auth
});
