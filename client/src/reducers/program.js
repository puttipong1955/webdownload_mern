import {
  GET_PROGRAMS_HIT,
  PROGRAMS_ERROR,
  CLEAR_PROGRAMS,
  GET_PROGRAMS_LAST_LIMIT,
  GET_PROGRAM_CATEGORIES,
  GET_PROGRAMS,
  GET_PROGRAM_BY_ID,
  GET_PROGRAMS_BY_CATEGORY,
  GET_SEARCH,
  GET_PROGRAMS_RANDOM,
  GET_PROGRAMS_RANDOM_BY_CATEGORY,
  PUT_PROGRAM_DOWNLOAD,
  POST_PROGRAM,
  PUT_PROGRAM,
  GET_TAG
} from "../actions/types";

const initialState = {
  program: {},
  programs: [],
  programs_hit: [],
  programs_limit: [],
  pro_categories: [],
  programs_category: [],
  programs_random: [],
  search: [],
  tag: [],
  programs_random_category: [],
  download: null,
  loading: true,
  error: {}
};

export default function(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_PROGRAMS_HIT:
      return {
        ...state,
        programs_hit: payload,
        loading: false
      };
    case GET_PROGRAMS_LAST_LIMIT:
      return {
        ...state,
        programs_limit: payload,
        loading: false
      };
    case GET_PROGRAM_CATEGORIES:
      return {
        ...state,
        pro_categories: payload,
        loading: false
      };
    case GET_PROGRAMS:
      return {
        ...state,
        programs: payload,
        loading: false
      };
    case POST_PROGRAM:
    case GET_PROGRAM_BY_ID:
    case PUT_PROGRAM:
      return {
        ...state,
        program: payload,
        loading: false
      };
    case GET_PROGRAMS_BY_CATEGORY:
      return {
        ...state,
        programs_category: payload,
        loading: false
      };
    case GET_PROGRAMS_RANDOM:
      return {
        ...state,
        programs_random: payload,
        loading: false
      };
    case GET_SEARCH:
      return {
        ...state,
        search: payload,
        loading: false
      };
    case GET_TAG:
      return {
        ...state,
        tag: payload,
        loading: false
      };
    case GET_PROGRAMS_RANDOM_BY_CATEGORY:
      return {
        ...state,
        programs_random_category: payload,
        loading: false
      };
    case PUT_PROGRAM_DOWNLOAD:
      return {
        ...state,
        download: payload,
        loading: false
      };
    case PROGRAMS_ERROR:
      return {
        ...state,
        error: payload,
        loading: false
      };

    case CLEAR_PROGRAMS:
      return {
        ...state,
        program: {},
        programs: [],
        programs_hit: [],
        programs_limit: [],
        pro_categories: [],
        programs_category: [],
        search: [],
        tag: [],
        programs_random: [],
        programs_random_category: [],
        download: null,
        loading: true
      };

    default:
      return state;
  }
}
