import {
  GET_GAMES_LAST,
  PROGRAMS_ERROR,
  CLEAR_PROGRAMS,
  GET_GAMES_LAST_LIMIT,
  GET_GAMES_MOST,
  GET_GAMES,
  GET_GAME_BY_ID,
  GET_GAMES_BY_CATEGORY,
  GET_LINKS_GAME,
  PUT_GAME_DOWNLOAD,
  POST_GAME,
  PUT_GAME,
  POST_LINK
} from "../actions/types";

const initialState = {
  game: {},
  games: [],
  games_hit: [],
  games_limit: [],
  games_most: [],
  games_category: [],
  game_links: [],
  download: null,
  loading: true,
  error: {}
};

export default function(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_GAMES_LAST:
      return {
        ...state,
        games_hit: payload,
        loading: false
      };
    case GET_GAMES_LAST_LIMIT:
      return {
        ...state,
        games_limit: payload,
        loading: false
      };
    case GET_GAMES_MOST:
      return {
        ...state,
        games_most: payload,
        loading: false
      };
    case GET_GAMES:
      return {
        ...state,
        games: payload,
        loading: false
      };
    case GET_GAME_BY_ID:
    case POST_GAME:
    case PUT_GAME:
      return {
        ...state,
        game: payload,
        loading: false
      };
    case GET_GAMES_BY_CATEGORY:
      return {
        ...state,
        games_category: payload,
        loading: false
      };
    case GET_LINKS_GAME:
      return {
        ...state,
        game_links: payload,
        loading: false
      };
    case PUT_GAME_DOWNLOAD:
      return {
        ...state,
        download: payload,
        loading: false
      };
    case PROGRAMS_ERROR:
      return {
        ...state,
        error: payload,
        loading: false
      };

    case CLEAR_PROGRAMS:
      return {
        ...state,
        game: {},
        games: [],
        games_hit: [],
        games_limit: [],
        games_most: [],
        games_category: [],
        game_links: [],
        download: null,
        loading: true
      };

    default:
      return state;
  }
}
