import React from "react";
import { Route, Switch } from "react-router-dom";
import PrivateRout from "../components/layouts/PrivateRoute";
import Programs from "../components/programs/Programs";
import Applications from "../components/applications/Applications";
import Games from "../components/games/Games";
import Program from "../components/program/Program";
import Application from "../components/application/Application";
import Game from "../components/game/Game";
import ProgramsCategory from "../components/programs_category/ProgramsCategory";
import ApplicationsCategory from "../components/applications_category/ApplicationsCategory";
import Search from "../components/search/Search";
import GamesCategory from "../components/games_category/GamesCategory";
import test from "../components/games/Games";
import AdminPrograms from "../components/admin_programs/AdminPrograms";
import AdminApplications from "../components/admin_applications/AdminApplications";
import AdminGames from "../components/admin_games/AdminGames";
import AdminProgramAdd from "../components/admin_programs/AdminProgramAdd";
import AdminProgramEdit from "../components/admin_programs/AdminProgramEdit";
import AdminApplicationAdd from "../components/admin_applications/AdminApplicationAdd";
import AdminApplicationEdit from "../components/admin_applications/AdminApplicationEdit";
import AdminGameAdd from "../components/admin_games/AdminGameAdd";
import AdminGameEdit from "../components/admin_games/AdminGameEdit";
import AdminGameLinks from "../components/admin_games/AdminGameLinks";
import AdminGameLinkDel from "../components/admin_games/AdminGameLinkDel";
import AdminGameLogin from "../components/admin_login/AdminLogin";
import AdminHome from "../components/admin_home/AdminHome";
import Tag from "../components/tag/Tag";

const Routing = () => {
  return (
    <Switch>
      <Route exact path="/programs" component={Programs} />
      <Route exact path="/applications" component={Applications} />
      <Route exact path="/games" component={Games} />
      <Route exact path="/program/:category/:id" component={Program} />
      <Route exact path="/application/:category/:id" component={Application} />
      <Route exact path="/game/:id" component={Game} />
      <Route
        exact
        path="/programs/category/:category"
        component={ProgramsCategory}
      />
      <Route
        exact
        path="/applications/category/:category"
        component={ApplicationsCategory}
      />
      <Route exact path="/programs/search/:text" component={Search} />
      <Route exact path="/tag/:text" component={Tag} />
      <Route exact path="/games/category/:category" component={GamesCategory} />
      <Route exact path="/test" component={test} />

      <PrivateRout exact path="/admin/programs" component={AdminPrograms} />
      <PrivateRout
        exact
        path="/admin/applications"
        component={AdminApplications}
      />
      <PrivateRout exact path="/admin/games" component={AdminGames} />
      <PrivateRout
        exact
        path="/admin/program/add"
        component={AdminProgramAdd}
      />
      <PrivateRout
        exact
        path="/admin/program/:id"
        component={AdminProgramEdit}
      />
      <PrivateRout
        exact
        path="/admin/application/add"
        component={AdminApplicationAdd}
      />
      <PrivateRout
        exact
        path="/admin/application/:id"
        component={AdminApplicationEdit}
      />
      <PrivateRout exact path="/admin/game/add" component={AdminGameAdd} />
      <PrivateRout exact path="/admin/game/:id" component={AdminGameEdit} />
      <PrivateRout
        exact
        path="/admin/game/links/:id"
        component={AdminGameLinks}
      />
      <PrivateRout
        exact
        path="/admin/game/link/del/:id_game/:id"
        component={AdminGameLinkDel}
      />
      <Route exact path="/admin" component={AdminGameLogin} />
      <PrivateRout exact path="/admin/dashboard" component={AdminHome} />
    </Switch>
  );
};

export default Routing;
