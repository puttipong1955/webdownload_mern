export const GET_PROGRAMS_HIT = "GET_PROGRAMS_HIT";
export const PROGRAMS_ERROR = "PROGRAMS_ERROR";
export const CLEAR_PROGRAMS = "CLEAR_PROGRAMS";
export const GET_APPLICATION_HIT = "GET_APPLICATION_HIT";
export const GET_GAMES_LAST = "GET_GAMES_LAST";
export const GET_PROGRAMS_LAST_LIMIT = "GET_PROGRAMS_LAST_LIMIT";
export const GET_APPLICATIONS_LAST_LIMIT = "GET_APPLICATIONS_LAST_LIMIT";
export const GET_GAMES_LAST_LIMIT = "GET_GAMES_LAST_LIMIT";
export const GET_PROGRAM_CATEGORIES = "GET_PROGRAM_CATEGORIES";
export const GET_APPLICATION_CATEGORIES = "GET_APPLICATION_CATEGORIES";
export const GET_PROGRAMS = "GET_PROGRAMS";
export const GET_APPLICATIONS = "GET_APPLICATIONS";
export const GET_GAMES_MOST = "GET_GAMES_MOST";
export const GET_GAMES = "GET_GAMES";
export const GET_PROGRAM_BY_ID = "GET_PROGRAM_BY_ID";
export const GET_APPLICATION_BY_ID = "GET_APPLICATION_BY_ID";
export const GET_GAME_BY_ID = "GET_GAME_BY_ID";
export const GET_PROGRAMS_BY_CATEGORY = "GET_PROGRAMS_BY_CATEGORY";
export const GET_APPLICATIONS_BY_CATEGORY = "GET_APPLICATIONS_BY_CATEGORY";
export const GET_GAMES_BY_CATEGORY = "GET_GAMES_BY_CATEGORY";
export const GET_SEARCH = "GET_SEARCH";
export const GET_TAG = "GET_TAG";
export const GET_PROGRAMS_RANDOM = "GET_PROGRAMS_RANDOM";
export const GET_APPLICATIONS_RANDOM = "GET_APPLICATIONS_RANDOM";
export const GET_PROGRAMS_RANDOM_BY_CATEGORY =
  "GET_PROGRAMS_RANDOM_BY_CATEGORY";
export const GET_APPLICATIONS_RANDOM_BY_CATEGORY =
  "GET_APPLICATIONS_RANDOM_BY_CATEGORY";
export const PUT_PROGRAM_DOWNLOAD = "PUT_PROGRAM_DOWNLOAD";
export const GET_LINKS_GAME = "GET_LINKS_GAME";
export const PUT_GAME_DOWNLOAD = "PUT_GAME_DOWNLOAD";

//Admin
export const POST_PROGRAM = "POST_PROGRAM";
export const DELETE_PROGRAM = "DELETE_PROGRAM";
export const PUT_PROGRAM = "PUT_PROGRAM";
export const POST_APPLICATION = "POST_APPLICATION";
export const DELETE_APPLICATION = "DELETE_PROGRAM";
export const PUT_APPLICATION = "PUT_PROGRAM";
export const POST_GAME = "POST_GAME";
export const PUT_GAME = "PUT_GAME";
export const POST_LINK = "POST_LINK";
export const LOGIN_FAIL = "LOGIN_FAIL";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGOUT = "LOGOUT";
export const USER_LOADED = "USER_LOADED";
export const AUTH_ERROR = "AUTH_ERROR";
