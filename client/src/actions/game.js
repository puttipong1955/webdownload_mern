import axios from "axios";
import {
  GET_GAMES_LAST,
  PROGRAMS_ERROR,
  CLEAR_PROGRAMS,
  GET_GAMES_LAST_LIMIT,
  GET_GAMES_MOST,
  GET_GAMES,
  GET_GAME_BY_ID,
  GET_GAMES_BY_CATEGORY,
  GET_LINKS_GAME,
  PUT_GAME_DOWNLOAD,
  POST_GAME,
  PUT_GAME,
  POST_LINK
} from "./types";

// Get games limit 5
export const getGamesLast = () => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get("/api/games/last");

    dispatch({
      type: GET_GAMES_LAST,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Get games limit 15
export const getGamesLastLimit = () => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get("/api/games/limit");

    dispatch({
      type: GET_GAMES_LAST_LIMIT,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Get games most 15
export const getGamesMostLimit = () => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get("/api/games/most");

    dispatch({
      type: GET_GAMES_MOST,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Get games last
export const getGames = () => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get("/api/games");

    dispatch({
      type: GET_GAMES,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Get games by id
export const getGameById = id => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get(`/api/games/${id}`);

    dispatch({
      type: GET_GAME_BY_ID,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Get games by category
export const getGameByCategory = category => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get(`/api/games/category/${category}`);

    dispatch({
      type: GET_GAMES_BY_CATEGORY,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Get game links
export const getLinksGame = id => async dispatch => {
  try {
    const res = await axios.get(`/api/games/links/${id}`);

    dispatch({
      type: GET_LINKS_GAME,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Put game download
export const putGameDownload = id => async dispatch => {
  try {
    const res = await axios.put(`/api/games/download/${id}`);

    dispatch({
      type: PUT_GAME_DOWNLOAD,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// delete game
export const deleteGame = id => dispatch => {
  try {
    axios.delete(`/api/games/${id}`);
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// post game
export const postGame = userData => dispatch => {
  try {
    const res = axios.post("/api/games", userData).then(alert("เรียบร้อย"));

    dispatch({
      type: POST_GAME,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// put game
export const putGame = (id, userData) => dispatch => {
  try {
    const res = axios
      .put(`/api/games/${id}`, userData)
      .then(alert("แก้ไขเรียบร้อย"));

    dispatch({
      type: PUT_GAME,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// post link
export const postGameLink = (id, userData) => dispatch => {
  try {
    axios
      .post(`/api/games/link/${id}`, userData)
      .then(alert("เรียบร้อย"))
      .then(window.location.reload());
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// delete game links
export const deleteGameLinks = id => dispatch => {
  try {
    axios.delete(`/api/games/links/${id}`);
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// delete game link
export const deleteGameLink = id => dispatch => {
  try {
    axios.delete(`/api/games/link/${id}`);
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};
