import axios from "axios";
import {
  LOGIN_FAIL,
  USER_LOADED,
  LOGIN_SUCCESS,
  LOGOUT,
  PROGRAMS_ERROR,
  AUTH_ERROR
} from "./types";
import setAuthToken from "../utils/setAuthToken";
import { Redirect } from "react-router-dom";

// Load User
export const loadUser = () => async dispatch => {
  if (localStorage.token) {
    setAuthToken(localStorage.token);
  }
  try {
    const res = await axios.get("/api/auth");

    dispatch({
      type: USER_LOADED,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: AUTH_ERROR
    });
  }
};

// Login User
export const login = (email, password) => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  const body = JSON.stringify({ email, password });

  try {
    const res = await axios.post("/api/auth", body, config);

    dispatch({
      type: LOGIN_SUCCESS,
      payload: res.data
    });

    dispatch(loadUser());
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
    dispatch({
      type: LOGIN_FAIL
    });
  }
};

// LOGOUT /Clear Profile
export const logout = () => dispatch => {
  dispatch({ type: LOGOUT });
};
