import axios from "axios";
import {
  GET_PROGRAMS_HIT,
  PROGRAMS_ERROR,
  CLEAR_PROGRAMS,
  GET_PROGRAMS_LAST_LIMIT,
  GET_PROGRAM_CATEGORIES,
  GET_PROGRAMS,
  GET_PROGRAM_BY_ID,
  GET_PROGRAMS_BY_CATEGORY,
  GET_SEARCH,
  GET_PROGRAMS_RANDOM,
  GET_PROGRAMS_RANDOM_BY_CATEGORY,
  PUT_PROGRAM_DOWNLOAD,
  POST_PROGRAM,
  GET_TAG,
  PUT_PROGRAM
} from "./types";

// Get  programs
export const getProgramsHit = () => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get("/api/programs/populars");

    dispatch({
      type: GET_PROGRAMS_HIT,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Get programs last limit
export const getProgramsLastLimit = () => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get("/api/programs/limit");

    dispatch({
      type: GET_PROGRAMS_LAST_LIMIT,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Get programs categories
export const getProgramsCategories = () => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get("/api/programs/category/group");

    dispatch({
      type: GET_PROGRAM_CATEGORIES,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Get programs
export const getPrograms = () => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get("/api/programs");

    dispatch({
      type: GET_PROGRAMS,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Get programs by id
export const getProgramsById = id => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get(`/api/programs/${id}`);

    dispatch({
      type: GET_PROGRAM_BY_ID,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Get programs by category
export const getProgramsByCategory = category => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get(`/api/programs/category/${category}`);

    dispatch({
      type: GET_PROGRAMS_BY_CATEGORY,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Get search programs and applications and games
export const getSearch = key => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get(`/api/programs/search/${key}`);

    dispatch({
      type: GET_SEARCH,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Get search programs ramdom limit 10
export const getProgramsRamdom10 = () => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get(`/api/programs/random`);

    dispatch({
      type: GET_PROGRAMS_RANDOM,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Get programs ramdom by category limit 3
export const getProgramsRamdomByCategory3 = category => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get(`/api/programs/category/${category}/random`);

    dispatch({
      type: GET_PROGRAMS_RANDOM_BY_CATEGORY,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Put program download
export const putProgramDownload = id => async dispatch => {
  try {
    const res = await axios.put(`/api/programs/download/${id}`);

    dispatch({
      type: PUT_PROGRAM_DOWNLOAD,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// post program
export const postProgram = userData => dispatch => {
  try {
    const res = axios.post("/api/programs", userData).then(alert("เรียบร้อย"));

    dispatch({
      type: POST_PROGRAM,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// delete program
export const deleteProgram = id => dispatch => {
  try {
    axios.delete(`/api/programs/${id}`);
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// put program
export const putProgram = (id, userData) => dispatch => {
  try {
    const res = axios
      .put(`/api/programs/${id}`, userData)
      .then(alert("แก้ไขเรียบร้อย"));

    dispatch({
      type: PUT_PROGRAM,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Get tag programs and applications and games
export const getTag = key => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get(`/api/programs/tag/${key}`);

    dispatch({
      type: GET_TAG,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};