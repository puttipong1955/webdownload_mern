import axios from "axios";
import {
  GET_APPLICATION_HIT,
  PROGRAMS_ERROR,
  CLEAR_PROGRAMS,
  GET_APPLICATIONS_LAST_LIMIT,
  GET_APPLICATION_CATEGORIES,
  GET_APPLICATIONS,
  GET_APPLICATION_BY_ID,
  GET_APPLICATIONS_BY_CATEGORY,
  GET_APPLICATIONS_RANDOM,
  GET_APPLICATIONS_RANDOM_BY_CATEGORY,
  POST_APPLICATION,
  DELETE_APPLICATION,
  PUT_APPLICATION
} from "./types";

// Get  applications popular
export const getApplicationsHit = () => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get("/api/applications/populars");

    dispatch({
      type: GET_APPLICATION_HIT,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Get  applications limit
export const getApplicationsLastLimit = () => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get("/api/applications/limit");

    dispatch({
      type: GET_APPLICATIONS_LAST_LIMIT,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Get  applications categories
export const getApplicationsCategories = () => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get("/api/applications/category/group");

    dispatch({
      type: GET_APPLICATION_CATEGORIES,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Get  applications
export const getApplications = () => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get("/api/applications");

    dispatch({
      type: GET_APPLICATIONS,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Get  application by id
export const getApplicationById = id => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get(`/api/applications/${id}`);

    dispatch({
      type: GET_APPLICATION_BY_ID,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Get  application by category
export const getApplicationByCategory = category => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get(`/api/applications/category/${category}`);

    dispatch({
      type: GET_APPLICATIONS_BY_CATEGORY,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Get  application random limit 10
export const getApplicationRandom10 = () => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get(`/api/applications/random`);

    dispatch({
      type: GET_APPLICATIONS_RANDOM,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};

// Get  application random by category limit 3
export const getApplicationRandomByCategory3 = category => async dispatch => {
  dispatch({ type: CLEAR_PROGRAMS });
  try {
    const res = await axios.get(
      `/api/applications/category/${category}/random`
    );

    dispatch({
      type: GET_APPLICATIONS_RANDOM_BY_CATEGORY,
      payload: res.data
    });
  } catch (err) {
    dispatch({
      type: PROGRAMS_ERROR,
      payload: { msg: err.response.statusText, Status: err.response.status }
    });
  }
};
