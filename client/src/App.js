import React, { Fragment, useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Navbar from "./components/layouts/Nav";
import Footer from "./components/layouts/Footer";
import Routing from "./routes/Routing";
import Home from "./components/home/Home";
import { Helmet } from "react-helmet";

//Redux
import { Provider } from "react-redux";
import store from "./store";

const App = () => {
  return (
    <div>
      <Helmet>
        <title>Turbo Todo</title>
        <meta name="description" content="Todos on steroid!" />
        <meta name="theme-color" content="#008f68" />
      </Helmet>
      <Provider store={store}>
        <Router>
          <Fragment>
            <Navbar />
            <Switch>
              <Route exact path="/" component={Home} />
              <Route component={Routing} />
            </Switch>
            <Footer />
          </Fragment>
        </Router>
      </Provider>
    </div>
  );
};

export default App;
