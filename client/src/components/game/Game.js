import React, { Fragment, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { getGameById, getLinksGame, putGameDownload } from "../../actions/game";
import Moment from "moment";
import Spinner from "../layouts/Spinner";
import Carousel from "./Carousel";

const Game = ({
  getGameById,
  getLinksGame,
  putGameDownload,
  game: { game, game_links, loading },
  match
}) => {
  const [tags, setTags] = useState("tags");

  useEffect(() => {
    window.scrollTo(0, 0);
    getGameById(match.params.id);
    getLinksGame(match.params.id);
  }, [getGameById, getLinksGame]);

  let dateCreate = Moment(game.create_date)
    .utc()
    .format("YYYY-MM-DD");

  const explode = function() {
    if (tags === "tags") setTags(game.tag);
  };
  setTimeout(explode, 1200);

  const OnelinkDownload = link => {
    if (link.type === 1) {
      return (
        <Link to={link.link} target="_blank">
          <button className="btn btn-sm btn-primary" onClick={updateDownload}>
            <i className="fas fa-download" />
            DOWNLOAD 1 PART
          </button>
        </Link>
      );
    }
  };

  const linkDownload = link => {
    if (link.type === 2) {
      return (
        <Link to={link.link} target="_blank">
          <button className="btn btn-sm btn-success" onClick={updateDownload}>
            <i className="fas fa-long-arrow-alt-down" />
            PART{link.num}
          </button>
        </Link>
      );
    }
  };

  const CrackDownload = link => {
    if (link.type === 3) {
      return (
        <Link to={link.link} target="_blank">
          <button className="btn btn-sm btn-warning" onClick={updateDownload}>
            <i className="fas fa-cog" />
            Crack
          </button>
        </Link>
      );
    }
  };

  const PatchDownload = link => {
    if (link.type === 4) {
      return (
        <Link to={link.link} target="_blank">
          <button
            style={{ marginLeft: "2px" }}
            className="btn btn-sm btn-danger"
            onClick={updateDownload}
          >
            <i className="fas fa-file" />
            Patch
          </button>
        </Link>
      );
    }
  };

  const updateDownload = () => {
    putGameDownload(match.params.id);
  };

  const info = { __html: game.info };

  return (
    <Fragment>
      {loading ? (
        <Spinner />
      ) : (
        <Fragment>
          <section className="section-game-content">
            <div className="contianer-lg">
              <div className="row">
                <div className="col col-sm-6 content desktop">
                  <div className="games-gard">
                    <div className="games-image" alt="">
                      <img src={game.icon} />
                      <h2>{game.name}</h2>
                    </div>
                  </div>
                  <div className="info">
                    <ul>
                      <li>
                        <div className="icon">
                          <i className="far fa-building" />
                        </div>
                        <div className="text">
                          <p>
                            <b>บริษัท: </b>
                            {game.company}
                          </p>
                        </div>
                      </li>
                      <li>
                        <div className="icon">
                          <i className="fas fa-calendar" />
                        </div>
                        <div className="text">
                          <p>
                            <b>ออกจำหน่าย:</b> {dateCreate}
                          </p>
                        </div>
                      </li>
                      <li>
                        <div className="icon">
                          <i className="fas fa-file" />
                        </div>
                        <div className="text">
                          <p>
                            <b>ขนาดไฟล์:</b> {game.size}
                          </p>
                        </div>
                      </li>
                      <li>
                        <div className="icon">
                          <i className="fas fa-tag" />
                        </div>
                        <div className="text">
                          <p>
                            <Link to={`/games/category/${game.categorie}`}>
                              {game.categorie}
                            </Link>
                          </p>
                        </div>
                      </li>
                    </ul>
                    <br />
                    <ul className>
                      <li>
                        <div className="text">
                          <p>
                            <u>สเปคขั้นต่ำ</u>
                          </p>
                        </div>
                      </li>
                      <li>
                        <div className="icon">
                          <i className="fab fa-windows" />
                        </div>
                        <div className="text">
                          <p>
                            <b>OS: </b>
                            {game.os}
                          </p>
                        </div>
                      </li>
                      <li>
                        <div className="icon">
                          <i className="fas fa-microchip" />
                        </div>
                        <div className="text">
                          <p>
                            <b>CPU:</b> {game.cpu}
                          </p>
                        </div>
                      </li>
                      <li>
                        <div className="icon">
                          <i className="fas fa-memory" />
                        </div>
                        <div className="text">
                          <p>
                            <b>RAM:</b> {game.ram}
                          </p>
                        </div>
                      </li>
                      <li>
                        <div className="icon">
                          <i className="fas fa-ruler" />
                          "&gt;
                        </div>
                        <div className="text">
                          <p>
                            <b>GPU:</b> {game.grafic}
                          </p>
                        </div>
                      </li>
                      <li>
                        <div className="icon">
                          <i className="fas fa-hdd" />
                        </div>
                        <div className="text">
                          <p>
                            <b>HDD: </b>
                            {game.disk}
                          </p>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>

                <div className="col col-sm-12 content phone">
                  <div className="games-gard">
                    <div className="games-image" alt="">
                      <img src={game.icon} />
                      <h2>{game.name}</h2>
                    </div>
                  </div>
                </div>

                <div className="col col-sm-12 content phone">
                  <div className="info">
                    <ul>
                      <li>
                        <div className="icon">
                          <i className="far fa-building" />
                        </div>
                        <div className="text">
                          <p>
                            <b>บริษัท: </b>
                            {game.company}
                          </p>
                        </div>
                      </li>
                      <li>
                        <div className="icon">
                          <i className="fas fa-calendar" />
                        </div>
                        <div className="text">
                          <p>
                            <b>ออกจำหน่าย:</b> {dateCreate}
                          </p>
                        </div>
                      </li>
                      <li>
                        <div className="icon">
                          <i className="fas fa-file" />
                        </div>
                        <div className="text">
                          <p>
                            <b>ขนาดไฟล์:</b> {game.size}
                          </p>
                        </div>
                      </li>
                      <li>
                        <div className="icon">
                          <i className="fas fa-tag" />
                        </div>
                        <div className="text">
                          <p>
                            <Link to={`/games/category/${game.categorie}`}>
                              {game.categorie}
                            </Link>
                          </p>
                        </div>
                      </li>
                    </ul>
                    <br />
                    <ul className>
                      <li>
                        <div className="text">
                          <p>
                            <u>สเปคขั้นต่ำ</u>
                          </p>
                        </div>
                      </li>
                      <li>
                        <div className="icon">
                          <i className="fab fa-windows" />
                        </div>
                        <div className="text">
                          <p>
                            <b>OS: </b>
                            {game.os}
                          </p>
                        </div>
                      </li>
                      <li>
                        <div className="icon">
                          <i className="fas fa-microchip" />
                        </div>
                        <div className="text">
                          <p>
                            <b>CPU:</b> {game.cpu}
                          </p>
                        </div>
                      </li>
                      <li>
                        <div className="icon">
                          <i className="fas fa-memory" />
                        </div>
                        <div className="text">
                          <p>
                            <b>RAM:</b> {game.ram}
                          </p>
                        </div>
                      </li>
                      <li>
                        <div className="icon">
                          <i className="fas fa-ruler" />
                          "&gt;
                        </div>
                        <div className="text">
                          <p>
                            <b>GPU:</b> {game.grafic}
                          </p>
                        </div>
                      </li>
                      <li>
                        <div className="icon">
                          <i className="fas fa-hdd" />
                        </div>
                        <div className="text">
                          <p>
                            <b>HDD: </b>
                            {game.disk}
                          </p>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>

                <div className="col col-sm-6 media">
                  <div className="video">
                    <iframe
                      width="100%"
                      height={400}
                      src={`https://www.youtube.com/embed/${game.video}?rel=0&autoplay=1&mute=1`}
                    ></iframe>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section className="section-game-content2">
            <div className="contianer-lg">
              <div className="row">
                <div className="col col-sm-7 contents desktop">
                  <div className="content">
                    <p dangerouslySetInnerHTML={info}></p>
                  </div>
                  <div className="download">
                    <h1>DOWNLOAD {game.name}</h1>
                    <ul>
                      <li>
                        <div className="one-part">
                          <sub>*โหลดไฟล์เดียว</sub>
                          {game_links.map(link => OnelinkDownload(link))}
                        </div>
                      </li>
                      <li>
                        <div className="part">
                          <sub>*โหลดแบ่งไฟล์</sub>
                          {game_links.map(link => linkDownload(link))}
                        </div>
                      </li>
                      <li>
                        <div className="crack">
                          {game_links.map(link => CrackDownload(link))}
                          {game_links.map(link => PatchDownload(link))}
                        </div>
                      </li>
                      <li></li>
                    </ul>
                  </div>
                  <div className="tags">
                    <p className="tag">Tags</p>
                    <p className="tagArr">
                      {tags
                        ? tags.split(",").map(tag => (
                            <Link to={`/tag/${tag.trim()}`}>
                              <i class="far fa-dot-circle fa-xs"></i>{" "}
                              {tag.trim()}{" "}
                            </Link>
                          ))
                        : "Empty Tags"}
                    </p>
                  </div>
                  <button
                    className="btn btn-dark"
                    style={{
                      marginLeft: "30px",
                      marginTop: "10px",
                      marginBottom: "10px",
                      color: "white"
                    }}
                  >
                    <i class="fas fa-angle-left"></i>{" "}
                    <Link to={`/games`} style={{ color: "white" }}>
                      {" กลับ"}
                    </Link>
                  </button>
                </div>
                <div className="col col-sm-5 media desktop">
                  <div className=" carousel">
                    <Carousel
                      pic={game.pic}
                      pic2={game.pic2}
                      pic3={game.pic3}
                      pic4={game.pic4}
                    />
                  </div>
                </div>

                <div className="col col-sm-12 media phone">
                  <div className=" carousel">
                    <Carousel
                      pic={game.pic}
                      pic2={game.pic2}
                      pic3={game.pic3}
                      pic4={game.pic4}
                    />
                  </div>
                </div>
                <div className="col col-sm-12 contents phone">
                  <div className="content">
                    <p dangerouslySetInnerHTML={info}></p>
                  </div>
                  <div className="download">
                    <h1>DOWNLOAD {game.name}</h1>
                    <ul>
                      <li>
                        <div className="one-part">
                          <sub>*โหลดไฟล์เดียว</sub>
                          {game_links.map(link => OnelinkDownload(link))}
                        </div>
                      </li>
                      <li>
                        <div className="part">
                          <sub>*โหลดแบ่งไฟล์</sub>
                          {game_links.map(link => linkDownload(link))}
                        </div>
                      </li>
                      <li>
                        <div className="crack">
                          {game_links.map(link => CrackDownload(link))}
                          {game_links.map(link => PatchDownload(link))}
                        </div>
                      </li>
                      <li></li>
                    </ul>
                  </div>

                  <div className="tags">
                    <p className="tag">Tags</p>
                    <p className="tagArr">
                      {tags
                        ? tags.split(",").map(tag => (
                            <Link to={`/tag/${tag.trim()}`}>
                              <i class="far fa-dot-circle fa-xs"></i>{" "}
                              {tag.trim()}{" "}
                            </Link>
                          ))
                        : "Empty Tags"}
                    </p>
                  </div>
                  
                  <button
                    className="btn btn-dark"
                    style={{
                      marginLeft: "30px",
                      marginTop: "10px",
                      marginBottom: "10px",
                      color: "white"
                    }}
                  >
                    <i class="fas fa-angle-left"></i>{" "}
                    <Link to={`/games`} style={{ color: "white" }}>
                      {" กลับ"}
                    </Link>
                  </button>
                </div>
              </div>
            </div>
          </section>
        </Fragment>
      )}
    </Fragment>
  );
};

Game.propTypes = {
  getGameById: PropTypes.func.isRequired,
  getLinksGame: PropTypes.func.isRequired,
  putGameDownload: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  game: state.game
});

export default connect(mapStateToProps, {
  getGameById,
  getLinksGame,
  putGameDownload
})(Game);
