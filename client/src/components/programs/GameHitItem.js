import React from "react";
import Moment from "moment";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const GameHitItem = ({ id, icon, date }) => {
  let dateCreate = Moment(date)
    .utc()
    .format("YYYY-MM-DD");
  return (
    <div className="games-gard">
      <Link to={`/game/${id}`} target="_blank">
        <div className="games-image" alt="">
          <img src={icon} />
          <i className="calendar fa fa-calendar-alt" aria-hidden="false">
            {dateCreate}
          </i>
        </div>
      </Link>
    </div>
  );
};

GameHitItem.propTypes = {
  date: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired
};

export default GameHitItem;
