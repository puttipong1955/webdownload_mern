import React, { Fragment, useEffect, useState } from "react";
import PropTypes from "prop-types";
import "../../css/programs.css";
import { connect } from "react-redux";
import {
  getProgramsHit,
  getPrograms,
  getProgramsCategories
} from "../../actions/program";
import { getApplicationsHit } from "../../actions/application";
import { getGamesLastLimit } from "../../actions/game";
import Spinner from "../layouts/Spinner";
import Loadbar from '../layouts/Loadbar'

import ListItem from "./ListItem";
import CategoryItem from "./CategoryItem";
import ProgramHitItem from "./ProgramHitItem";
import GameHitItem from "./GameHitItem";
import ApplicationHitItem from "./ApplicationHitItem";
import Carousel from "../carousel/CarouselPrograms";
import { Button, Modal } from "react-bootstrap";

const Programs = ({
  getPrograms,
  getProgramsCategories,
  getProgramsHit,
  getApplicationsHit,
  getGamesLastLimit,
  program: { pro_categories, programs, programs_hit, loading },
  application: { applications_hit },
  game: { games_limit }
}) => {
  const [currentPage, setCurrentPage] = useState(1);
  const [todosPerPage, setTodosPerPage] = useState(10);
  const [upperPageBound, setUpperPageBound] = useState(10);
  const [lowerPageBound, setLowerPageBound] = useState(0);
  const [isPrevBtnActive, setIsPrevBtnActive] = useState("disabled");
  const [isNextBtnActive, setIsNextBtnActive] = useState("");
  const [pageBound, setPageBound] = useState(10);
  const [modalShow, setModalShow] = useState(false);

  useEffect(
    () => {
      window.scrollTo(0, 0);
      getPrograms();
      getProgramsCategories();
      getProgramsHit();
      getApplicationsHit();
      getGamesLastLimit();
    },
    [getPrograms],
    [getProgramsCategories],
    [getProgramsHit],
    [getApplicationsHit],
    [getGamesLastLimit]
  );

  const handleClick = event => {
    window.scrollTo(0, 0);
    let listid = Number(event.target.id);
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const setPrevAndNextBtnClass = listid => {
    let totalPage = Math.ceil(programs.length / todosPerPage);
    setIsNextBtnActive("disabled");
    setIsPrevBtnActive("disabled");
    if (totalPage === listid && totalPage > 1) {
      setIsPrevBtnActive("");
    } else if (listid === 1 && totalPage > 1) {
      setIsNextBtnActive("");
    } else if (totalPage > 1) {
      setIsNextBtnActive("");
      setIsPrevBtnActive("");
    }
  };

  const btnIncrementClick = () => {
    setUpperPageBound(upperPageBound + pageBound);
    setLowerPageBound(lowerPageBound + pageBound);
    let listid = upperPageBound + 1;
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const btnDecrementClick = () => {
    setUpperPageBound(upperPageBound - pageBound);
    setLowerPageBound(lowerPageBound - pageBound);
    let listid = upperPageBound - pageBound;
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const btnPrevClick = () => {
    if ((currentPage - 1) % pageBound === 0) {
      setUpperPageBound(upperPageBound - pageBound);
      setLowerPageBound(lowerPageBound - pageBound);
    }
    let listid = currentPage - 1;
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const btnNextClick = () => {
    if (currentPage + 1 > upperPageBound) {
      setUpperPageBound(upperPageBound + pageBound);
      setLowerPageBound(lowerPageBound + pageBound);
    }
    let listid = currentPage + 1;
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const indexOfLastTodo = currentPage * todosPerPage;
  const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
  const currentTodos = programs.slice(indexOfFirstTodo, indexOfLastTodo);

  const renderTodos = currentTodos.map((todo, index) => {
    return (
      <ListItem
        key={index}
        id={todo.id}
        category={todo.category}
        name={todo.name}
        text={todo.text}
        icon={todo.icon}
        date={todo.create_date}
        app={todo.application}
      />
    );
  });

  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(programs.length / todosPerPage); i++) {
    pageNumbers.push(i);
  }

  const renderPageNumbers = pageNumbers.map(number => {
    if (number === 1 && currentPage === 1) {
      return (
        <li key={number} className=" page-item" id={number}>
          <a id={number} onClick={handleClick} className="page-link">
            {number}
          </a>
        </li>
      );
    } else if (number < upperPageBound + 1 && number > lowerPageBound) {
      return (
        <li key={number} id={number} className="page-item">
          <a id={number} onClick={handleClick} className="page-link">
            {number}
          </a>
        </li>
      );
    }
  });
  let pageIncrementBtn = null;
  if (pageNumbers.length > upperPageBound) {
    pageIncrementBtn = (
      <li className="page-item">
        <a href="#" onClick={btnIncrementClick} className="page-link">
          {" "}
          &hellip;{" "}
        </a>
      </li>
    );
  }
  let pageDecrementBtn = null;
  if (lowerPageBound >= 1) {
    pageDecrementBtn = (
      <li className="page-item">
        <a href="#" onClick={btnDecrementClick} className="page-link">
          {" "}
          &hellip;{" "}
        </a>
      </li>
    );
  }
  let renderPrevBtn = null;
  if (isPrevBtnActive === "disabled") {
    renderPrevBtn = (
      <li className={`page-item ${isPrevBtnActive}`}>
        <a href="#" className="page-link">
          <span id="btnPrev "> {`<`}</span>
        </a>
      </li>
    );
  } else {
    renderPrevBtn = (
      <li className={`page-item ${isPrevBtnActive}`}>
        <a href="#" id="btnPrev" onClick={btnPrevClick} className="page-link">
          {" "}
          {`<`}{" "}
        </a>
      </li>
    );
  }
  let renderNextBtn = null;
  if (isNextBtnActive === "disabled") {
    renderNextBtn = (
      <li className={`page-item ${isNextBtnActive}`}>
        <a href="#" className="page-link">
          <span id="btnNext"> {`>`}</span>
        </a>
      </li>
    );
  } else {
    renderNextBtn = (
      <li className={`page-item ${isNextBtnActive}`}>
        <a href="#" id="btnNext" onClick={btnNextClick} className="page-link">
          {" "}
          {`>`}{" "}
        </a>
      </li>
    );
  }

  function ModalDialog(props) {
    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            หมวดหมู่โปรแกรม
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            {pro_categories.length > 0 ? (
              pro_categories.map(category => (
                <CategoryItem
                  key={category.category}
                  label={category.category}
                />
              ))
            ) : (
              <Spinner />
            )}
          </p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>ปิด</Button>
        </Modal.Footer>
      </Modal>
    );
  }

  return (
    <div>
      <Fragment>
        <section className="section-content">
          <div className="container-lg">
            <div className="row">
              <div className="col-md-2 cont1">
                <span className="label-cet">หมวดหมู่</span>
                <div className="list-group cetagories-list">
                  {pro_categories.length > 0
                    ? pro_categories.map(category => (
                        <CategoryItem
                          key={category.category}
                          label={category.category}
                        />
                      ))
                    : <Loadbar/>}
                </div>
              </div>
              <div className="col col-md-6 cont2">
                <h1 className="label-last">โปรแกรมใหม่ล่าสุด</h1>
                {loading ? (
                  <Spinner />
                ) : (
                  <Fragment>
                    <div className="list-items program-list">
                      <div className="list-group list-group-flush program-list-group ">
                        {renderTodos}
                        <div className="paginationBox">
                          <ul className="pagination" id="page-numbers">
                            {renderPrevBtn}
                            {pageDecrementBtn}
                            {renderPageNumbers}
                            {pageIncrementBtn}
                            {renderNextBtn}
                          </ul>
                        </div>
                      </div>
                    </div>
                  </Fragment>
                )}
              </div>
              <div className="col md col-1 dummy" />
              <div className="col md col-3 phone-hide">
                <div className="list-items list-more">
                  <h2 className="label-group">
                    โปรแกรมที่น่าสนใจ
                  </h2>
                  <div className="list-group program-list-more">
                    {programs_hit.length > 0 ? (
                      programs_hit.map(program => (
                        <ProgramHitItem
                          key={program.id}
                          id={program.id}
                          category={program.category}
                          name={program.name}
                          text={program.text}
                          icon={program.icon}
                        />
                      ))
                    ) : (
                      <Loadbar/>
                    )}
                  </div>
                </div>
                <div className="list-items list-more">
                  <h2 className="label-group">
                    แอพพลิเคชั่นที่น่าสนใจ
                  </h2>
                  <div className="list-group app-list">
                    {applications_hit.length > 0 ? (
                      applications_hit.map(application => (
                        <ApplicationHitItem
                          key={application.id}
                          id={application.id}
                          category={application.category}
                          name={application.name}
                          text={application.text}
                          icon={application.icon}
                        />
                      ))
                    ) : (
                      <Loadbar/>
                    )}
                  </div>
                </div>
                <div className="list-items list-more">
                  <h2 className="label-games">เกมส์ที่น่าสนใจ</h2>
                  <div className="list-games">
                    {games_limit.length > 0 ? (
                      games_limit
                        .slice(0, 6)
                        .map(game => (
                          <GameHitItem
                            key={game.id}
                            id={game.id}
                            icon={game.icon}
                            date={game.date}
                          />
                        ))
                    ) : (
                      <Loadbar/>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Button
            variant="primary modal-hide"
            onClick={() => setModalShow(true)}
          >
            <i class="fas fa-thumbtack"></i> หมวดหมู่
          </Button>
          <ModalDialog show={modalShow} onHide={() => setModalShow(false)} />
        </section>
        <div className="ramdom-show">
          <Carousel />
        </div>
      </Fragment>
    </div>
  );
};

Programs.propTypes = {
  getPrograms: PropTypes.func.isRequired,
  getProgramsCategories: PropTypes.func.isRequired,
  getProgramsHit: PropTypes.func.isRequired,
  getApplicationsHit: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  program: state.program,
  application: state.application,
  game: state.game
});

export default connect(mapStateToProps, {
  getPrograms,
  getProgramsCategories,
  getProgramsHit,
  getApplicationsHit,
  getGamesLastLimit
})(Programs);
