import React from "react";
import PropTypes from "prop-types";
import { Link, Redirect } from "react-router-dom";

const ProgramHitItem = ({ id, category, icon, name, text }) => {
  const newText = text.replace(/[<b></b><u></u><i></u>]/g, "");
  return (
    <Link
      to={`/program/${category}/${id}`}
      className="list-more-content list-group-item list-group-item-action"
      target="_blank"
    >
      <div className="list-content">
        <div className="list-image col">
          <img src={icon} />
        </div>
        <div className="list-text col">
          <span>{name}</span>
          <p>{newText}</p>
        </div>
      </div>
    </Link>
  );
};

ProgramHitItem.propTypes = {
  name: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired
};

export default ProgramHitItem;
