import React, { Fragment, useEffect, useState } from "react";
import PropTypes from "prop-types";
import "../../css/programs.css";
import { connect } from "react-redux";
import { getProgramsHit, putProgramDownload } from "../../actions/program";
import {
  getApplicationsHit,
  getApplicationsCategories,
  getApplicationById,
  getApplicationRandomByCategory3
} from "../../actions/application";
import Spinner from "../layouts/Spinner";

import CategoryItem from "./CategoryItem";
import ProgramHitItem from "./ProgramHitItem";
import ApplicationHitItem from "./ApplicationHitItem";
import Carousel from "./Carousel";
import ItemRandom from "./ItemRandom";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";

const Application = ({
  getApplicationById,
  getApplicationsCategories,
  getProgramsHit,
  getApplicationsHit,
  putProgramDownload,
  getApplicationRandomByCategory3,
  program: { programs_hit },
  application: {
    application,
    applications_hit,
    app_categories,
    applications_random_category,
    loading
  },
  match
}) => {
  const [tags, setTags] = useState("tags");

  useEffect(
    () => {
      window.scrollTo(0, 0);
      getApplicationById(match.params.id);
      getApplicationRandomByCategory3(match.params.category);
      getApplicationsCategories();
      getProgramsHit();
      getApplicationsHit();
    },
    [getApplicationById],
    [getApplicationsCategories],
    [getProgramsHit],
    [getApplicationsHit]
  );

  const putDownload = () => {
    putProgramDownload(match.params.id);
  };

  const text = { __html: application.text };
  const news = { __html: application.news };

  const explode = function() {
    if (tags === "tags") setTags(application.tag);
  };
  setTimeout(explode, 1200);

  return (
    <div>
      <Helmet>
        <meta charSet="utf-8" />
        <title>{application.name}</title>
      </Helmet>
      <Fragment>
        <section className="section-content">
          <div className="container-lg">
            <div className="row">
              <div className="col-md-2 cont1">
                <span className="label-cet">หมวดหมู่</span>
                <div className="list-group cetagories-list">
                  {app_categories.map(category => (
                    <CategoryItem
                      key={category.category}
                      label={category.category}
                    />
                  ))}
                </div>
              </div>
              <div className="col col-md-7 program-detail">
                {loading ? (
                  <Spinner />
                ) : (
                  <Fragment>
                    <img src={application.icon} />
                    <h1>{application.name}</h1>
                    <p dangerouslySetInnerHTML={text}></p>
                    <div className="carousel col col-sm-12 carousel-phone">
                      <Carousel pic={application.pic} pic2={application.pic2} />
                    </div>
                    <div className="carousel col col-md-8 col-8 carousel-desktop">
                      <Carousel pic={application.pic} pic2={application.pic2} />
                    </div>
                    <div className="col col-md-4 col-4 download">
                      <a href={application.link} target="_blank">
                        <button
                          className="btn btn-success"
                          onClick={putDownload}
                        >
                          DOWNLOAD
                          <i className="fas fa-file-download" />
                        </button>
                      </a>
                      <div className="download-info">
                        <ul className="list-group list-group-flush">
                          <li className="list-group-item">
                            ขนาด: {application.size}
                          </li>
                          <li className="list-group-item">
                            ระบบปฏิบัติการ: {application.os}
                          </li>
                          <li className="list-group-item">
                            เวอร์ชัน: {application.version}
                          </li>
                          <li className="list-group-item">
                            สิทธิ์การใช้งาน: {application.copyright}
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="what-new">
                      <h2 className="new">คุณสมบัติใหม่ในเวอร์ชั่นนี้</h2>
                      <p dangerouslySetInnerHTML={news}></p>
                    </div>
                    <div className="tags">
                      <h3 className="tag">Tags</h3>
                      <p>
                        {tags
                          ? tags.split(",").map(tag => (
                              <Link to={`/tag/${tag.trim()}`}>
                                <i class="far fa-dot-circle fa-xs"></i>{" "}
                                {tag.trim()}{" "}
                              </Link>
                            ))
                          : "Empty Tags"}
                      </p>
                    </div>

                    <p className="prosame">แอพพลิเคชั่นที่คล้ายกัน</p>
                    <div className="release-program">
                      {applications_random_category.map(app => (
                        <ItemRandom
                          key={app.id}
                          id={app.id}
                          name={app.name}
                          text={app.text}
                          icon={app.icon}
                          category={app.category}
                        />
                      ))}
                    </div>
                  </Fragment>
                )}
              </div>

              <div className="col md col-3">
                <div className="list-items list-more">
                  <a href="#" className="label-group">
                    แอพพลิเคชั่นที่น่าสนใจ
                  </a>
                  <div className="list-group app-list">
                    {applications_hit.length > 0 ? (
                      applications_hit.map(application => (
                        <ApplicationHitItem
                          key={application.id}
                          id={application.id}
                          category={application.category}
                          name={application.name}
                          text={application.text}
                          icon={application.icon}
                        />
                      ))
                    ) : (
                      <h6>ไม่พบรายการโปรแกรม...</h6>
                    )}
                  </div>
                </div>
                <div className="list-items list-more">
                  <a href="#" className="label-group">
                    โปรแกรมที่น่าสนใจ
                  </a>
                  <div className="list-group program-list-more">
                    {programs_hit.length > 0 ? (
                      programs_hit.map(program => (
                        <ProgramHitItem
                          key={program.id}
                          id={program.id}
                          category={program.category}
                          name={program.name}
                          text={program.text}
                          icon={program.icon}
                        />
                      ))
                    ) : (
                      <h6>ไม่พบรายการโปรแกรม...</h6>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </Fragment>
    </div>
  );
};

Application.propTypes = {
  getApplicationById: PropTypes.func.isRequired,
  getApplicationsCategories: PropTypes.func.isRequired,
  getProgramsHit: PropTypes.func.isRequired,
  getApplicationsHit: PropTypes.func.isRequired,
  getApplicationRandomByCategory3: PropTypes.func.isRequired,
  putProgramDownload: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  program: state.program,
  application: state.application
});

export default connect(mapStateToProps, {
  getApplicationById,
  getApplicationsCategories,
  getProgramsHit,
  getApplicationsHit,
  getApplicationRandomByCategory3,
  putProgramDownload
})(Application);
