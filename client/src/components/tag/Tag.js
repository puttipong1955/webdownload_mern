import React, { Fragment, useEffect, useState } from "react";
import PropTypes from "prop-types";
import "../../css/programs.css";
import { connect } from "react-redux";
import { getPrograms, getProgramsHit, getTag } from "../../actions/program";
import { getApplicationsHit } from "../../actions/application";
import { getGamesLastLimit } from "../../actions/game";
import Spinner from "../layouts/Spinner";
import Loadbar from "../layouts/Loadbar";

import ListItem from "../programs/ListItem";
import ProgramHitItem from "../programs/ProgramHitItem";
import ApplicationHitItem from "../programs/ApplicationHitItem";
import GameHitItem from "../programs/GameHitItem";

const Tag = ({
  getPrograms,
  getTag,
  getProgramsHit,
  getApplicationsHit,
  getGamesLastLimit,
  program: { programs_hit, tag, loading },
  application: { applications_hit },
  game: { games_limit },
  match
}) => {
  const [currentPage, setCurrentPage] = useState(1);
  const [todosPerPage, setTodosPerPage] = useState(10);
  const [upperPageBound, setUpperPageBound] = useState(10);
  const [lowerPageBound, setLowerPageBound] = useState(0);
  const [isPrevBtnActive, setIsPrevBtnActive] = useState("disabled");
  const [isNextBtnActive, setIsNextBtnActive] = useState("");
  const [pageBound, setPageBound] = useState(10);

  useEffect(
    () => {
      window.scrollTo(0, 0);
      getTag(match.params.text);
      getPrograms();
      getProgramsHit();
      getApplicationsHit();
      getGamesLastLimit();
    },
    [getPrograms],
    [getProgramsHit],
    [getApplicationsHit],
    [getGamesLastLimit],
    [getTag]
  );

  const handleClick = event => {
    window.scrollTo(0, 0);
    let listid = Number(event.target.id);
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const setPrevAndNextBtnClass = listid => {
    let totalPage = Math.ceil(tag.length / todosPerPage);
    setIsNextBtnActive("disabled");
    setIsPrevBtnActive("disabled");
    if (totalPage === listid && totalPage > 1) {
      setIsPrevBtnActive("");
    } else if (listid === 1 && totalPage > 1) {
      setIsNextBtnActive("");
    } else if (totalPage > 1) {
      setIsNextBtnActive("");
      setIsPrevBtnActive("");
    }
  };

  const btnIncrementClick = () => {
    setUpperPageBound(upperPageBound + pageBound);
    setLowerPageBound(lowerPageBound + pageBound);
    let listid = upperPageBound + 1;
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const btnDecrementClick = () => {
    setUpperPageBound(upperPageBound - pageBound);
    setLowerPageBound(lowerPageBound - pageBound);
    let listid = upperPageBound - pageBound;
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const btnPrevClick = () => {
    if ((currentPage - 1) % pageBound === 0) {
      setUpperPageBound(upperPageBound - pageBound);
      setLowerPageBound(lowerPageBound - pageBound);
    }
    let listid = currentPage - 1;
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const btnNextClick = () => {
    if (currentPage + 1 > upperPageBound) {
      setUpperPageBound(upperPageBound + pageBound);
      setLowerPageBound(lowerPageBound + pageBound);
    }
    let listid = currentPage + 1;
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const indexOfLastTodo = currentPage * todosPerPage;
  const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
  const currentTodos = tag.slice(indexOfFirstTodo, indexOfLastTodo);

  const renderTodos = currentTodos.map((todo, index) => {
    return (
      <ListItem
        key={index}
        id={todo.id}
        name={todo.name}
        text={todo.text}
        icon={todo.icon}
        category={todo.category}
        app={todo.application}
        date={todo.create_date}
      />
    );
  });

  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(tag.length / todosPerPage); i++) {
    pageNumbers.push(i);
  }

  const renderPageNumbers = pageNumbers.map(number => {
    if (number === 1 && currentPage === 1) {
      return (
        <li key={number} className=" page-item" id={number}>
          <a id={number} onClick={handleClick} className="page-link">
            {number}
          </a>
        </li>
      );
    } else if (number < upperPageBound + 1 && number > lowerPageBound) {
      return (
        <li key={number} id={number} className="page-item">
          <a id={number} onClick={handleClick} className="page-link">
            {number}
          </a>
        </li>
      );
    }
  });
  let pageIncrementBtn = null;
  if (pageNumbers.length > upperPageBound) {
    pageIncrementBtn = (
      <li className="page-item">
        <a href="#" onClick={btnIncrementClick} className="page-link">
          {" "}
          &hellip;{" "}
        </a>
      </li>
    );
  }
  let pageDecrementBtn = null;
  if (lowerPageBound >= 1) {
    pageDecrementBtn = (
      <li className="page-item">
        <a href="#" onClick={btnDecrementClick} className="page-link">
          {" "}
          &hellip;{" "}
        </a>
      </li>
    );
  }
  let renderPrevBtn = null;
  if (isPrevBtnActive === "disabled") {
    renderPrevBtn = (
      <li className={`page-item ${isPrevBtnActive}`}>
        <a href="#" className="page-link">
          <span id="btnPrev "> {`<`}</span>
        </a>
      </li>
    );
  } else {
    renderPrevBtn = (
      <li className={`page-item ${isPrevBtnActive}`}>
        <a href="#" id="btnPrev" onClick={btnPrevClick} className="page-link">
          {" "}
          {`<`}{" "}
        </a>
      </li>
    );
  }
  let renderNextBtn = null;
  if (isNextBtnActive === "disabled") {
    renderNextBtn = (
      <li className={`page-item ${isNextBtnActive}`}>
        <a href="#" className="page-link">
          <span id="btnNext"> {`>`}</span>
        </a>
      </li>
    );
  } else {
    renderNextBtn = (
      <li className={`page-item ${isNextBtnActive}`}>
        <a href="#" id="btnNext" onClick={btnNextClick} className="page-link">
          {" "}
          {`>`}{" "}
        </a>
      </li>
    );
  }

  return (
    <div>
      <Fragment>
        <section className="section-content">
          <div className="container-lg">
            <div className="row">
              <div className="col-md-2 cont1"></div>
              <div className="col col-md-6 cont2">
                <div style={{ height: "20px" }}></div>
                <h1 className="label-last" style={{ marginTop: "15px" }}>
                  Tag: " {match.params.text} "
                </h1>
                <Fragment>
                  {loading ? (
                    <Spinner />
                  ) : (
                    <Fragment>
                      <div className="list-items program-list">
                        <div className="list-group program-list-group">
                          {tag.length > 0 ? (
                            <Fragment>
                              {renderTodos}
                              {tag.length > 10 ? (
                                <div className="paginationBox">
                                  <ul className="pagination" id="page-numbers">
                                    {renderPrevBtn}
                                    {pageDecrementBtn}
                                    {renderPageNumbers}
                                    {pageIncrementBtn}
                                    {renderNextBtn}
                                  </ul>
                                </div>
                              ) : (
                                ""
                              )}
                            </Fragment>
                          ) : (
                            <h6
                              style={{
                                paddingTop: "10px",
                                color: "gray",
                                fontFamily: "roboto"
                              }}
                            >
                              ไม่พบผลลัพธ์การค้นหา...
                            </h6>
                          )}
                        </div>
                      </div>
                    </Fragment>
                  )}
                </Fragment>
              </div>
              <div className="col md col-1 dummy" />
              <div className="col md col-3 phone-hide">
                <div className="list-items list-more">
                  <h2 className="label-group">
                    โปรแกรมที่น่าสนใจ
                  </h2>
                  <div className="list-group program-list-more">
                    {programs_hit.length > 0 ? (
                      programs_hit.map(program => (
                        <ProgramHitItem
                          key={program.id}
                          id={program.id}
                          category={program.category}
                          name={program.name}
                          text={program.text}
                          icon={program.icon}
                        />
                      ))
                    ) : (
                      <Loadbar/>
                    )}
                  </div>
                </div>
                <div className="list-items list-more">
                  <h2 className="label-group">
                    แอพพลิเคชั่นที่น่าสนใจ
                  </h2>
                  <div className="list-group app-list">
                    {applications_hit.length > 0 ? (
                      applications_hit.map(application => (
                        <ApplicationHitItem
                          key={application.id}
                          id={application.id}
                          category={application.category}
                          name={application.name}
                          text={application.text}
                          icon={application.icon}
                        />
                      ))
                    ) : (
                      <Loadbar/>
                    )}
                  </div>
                </div>
                <h2 className="label-games" style={{marginTop: "5px"}}>เกมส์ที่น่าสนใจ</h2>

                <div className="list-games">
                  {games_limit.length > 0 ? (
                    games_limit
                      .slice(0, 6)
                      .map(game => (
                        <GameHitItem
                          key={game.id}
                          id={game.id}
                          icon={game.icon}
                          date={game.date}
                        />
                      ))
                  ) : (
                    <Loadbar/>
                  )}
                </div>
              </div>
            </div>
          </div>
        </section>
      </Fragment>
    </div>
  );
};

Tag.propTypes = {
  getPrograms: PropTypes.func.isRequired,
  getProgramsHit: PropTypes.func.isRequired,
  getApplicationsHit: PropTypes.func.isRequired,
  getTag: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  program: state.program,
  application: state.application,
  game: state.game
});

export default connect(mapStateToProps, {
  getPrograms,
  getProgramsHit,
  getApplicationsHit,
  getGamesLastLimit,
  getTag
})(Tag);
