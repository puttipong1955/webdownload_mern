import React, { Fragment, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getGameByCategory } from "../../actions/game";
import "../../css/games.css";
import Spinner from "../layouts/Spinner";
import GameItem from "./GameItem";
import { Link } from "react-router-dom";

const GamesCategory = ({
  getGameByCategory,
  game: { games_category, loading },
  match
}) => {
  const [currentPage, setCurrentPage] = useState(1);
  const [todosPerPage, setTodosPerPage] = useState(10);
  const [upperPageBound, setUpperPageBound] = useState(10);
  const [lowerPageBound, setLowerPageBound] = useState(0);
  const [isPrevBtnActive, setIsPrevBtnActive] = useState("disabled");
  const [isNextBtnActive, setIsNextBtnActive] = useState("");
  const [pageBound, setPageBound] = useState(10);

  useEffect(() => {
    window.scrollTo(0, 0);
    getGameByCategory(match.params.category);
  }, [getGameByCategory]);

  const handleClick = event => {
    window.scrollTo(0, 0);
    let listid = Number(event.target.id);
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const setPrevAndNextBtnClass = listid => {
    let totalPage = Math.ceil(games_category.length / todosPerPage);
    setIsNextBtnActive("disabled");
    setIsPrevBtnActive("disabled");
    if (totalPage === listid && totalPage > 1) {
      setIsPrevBtnActive("");
    } else if (listid === 1 && totalPage > 1) {
      setIsNextBtnActive("");
    } else if (totalPage > 1) {
      setIsNextBtnActive("");
      setIsPrevBtnActive("");
    }
  };

  const btnIncrementClick = () => {
    setUpperPageBound(upperPageBound + pageBound);
    setLowerPageBound(lowerPageBound + pageBound);
    let listid = upperPageBound + 1;
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const btnDecrementClick = () => {
    setUpperPageBound(upperPageBound - pageBound);
    setLowerPageBound(lowerPageBound - pageBound);
    let listid = upperPageBound - pageBound;
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const btnPrevClick = () => {
    if ((currentPage - 1) % pageBound === 0) {
      setUpperPageBound(upperPageBound - pageBound);
      setLowerPageBound(lowerPageBound - pageBound);
    }
    let listid = currentPage - 1;
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const btnNextClick = () => {
    if (currentPage + 1 > upperPageBound) {
      setUpperPageBound(upperPageBound + pageBound);
      setLowerPageBound(lowerPageBound + pageBound);
    }
    let listid = currentPage + 1;
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const indexOfLastTodo = currentPage * todosPerPage;
  const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
  const currentTodos = games_category.slice(indexOfFirstTodo, indexOfLastTodo);

  const renderTodos = currentTodos.map((todo, index) => {
    return (
      <GameItem
        key={index}
        id={todo.id}
        icon={todo.icon}
        date={todo.create_date}
        name={todo.name}
      />
    );
  });

  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(games_category.length / todosPerPage); i++) {
    pageNumbers.push(i);
  }

  const renderPageNumbers = pageNumbers.map(number => {
    if (number === 1 && currentPage === 1) {
      return (
        <li key={number} className=" page-item" id={number}>
          <a id={number} onClick={handleClick} className="page-link">
            {number}
          </a>
        </li>
      );
    } else if (number < upperPageBound + 1 && number > lowerPageBound) {
      return (
        <li key={number} id={number} className="page-item">
          <a id={number} onClick={handleClick} className="page-link">
            {number}
          </a>
        </li>
      );
    }
  });
  let pageIncrementBtn = null;
  if (pageNumbers.length > upperPageBound) {
    pageIncrementBtn = (
      <li className="page-item">
        <a href="#" onClick={btnIncrementClick} className="page-link">
          {" "}
          &hellip;{" "}
        </a>
      </li>
    );
  }
  let pageDecrementBtn = null;
  if (lowerPageBound >= 1) {
    pageDecrementBtn = (
      <li className="page-item">
        <a href="#" onClick={btnDecrementClick} className="page-link">
          {" "}
          &hellip;{" "}
        </a>
      </li>
    );
  }
  let renderPrevBtn = null;
  if (isPrevBtnActive === "disabled") {
    renderPrevBtn = (
      <li className={`page-item ${isPrevBtnActive}`}>
        <a href="#" className="page-link">
          <span id="btnPrev "> {`<`}</span>
        </a>
      </li>
    );
  } else {
    renderPrevBtn = (
      <li className={`page-item ${isPrevBtnActive}`}>
        <a href="#" id="btnPrev" onClick={btnPrevClick} className="page-link">
          {" "}
          {`<`}{" "}
        </a>
      </li>
    );
  }
  let renderNextBtn = null;
  if (isNextBtnActive === "disabled") {
    renderNextBtn = (
      <li className={`page-item ${isNextBtnActive}`}>
        <a href="#" className="page-link">
          <span id="btnNext"> {`>`}</span>
        </a>
      </li>
    );
  } else {
    renderNextBtn = (
      <li className={`page-item ${isNextBtnActive}`}>
        <a href="#" id="btnNext" onClick={btnNextClick} className="page-link">
          {" "}
          {`>`}{" "}
        </a>
      </li>
    );
  }

  return (
    <Fragment>
      {loading > 0 ? (
        <Spinner />
      ) : (
        <Fragment>
          <section class="section-most" style={{ marginTop: "-10px" }}>
            <div class="container-lg">
              <div class="row">
                <h1 class="game-label">หมวดหมู่: " {match.params.category} "</h1>
                <div class="games-content col-12">
                  <div>
                    <Fragment>
                      {games_category.length > 0 ? (
                        <Fragment>
                          {renderTodos}
                          {games_category.length > 10 ? (
                            <div className="paginationBox">
                              <ul className="pagination" id="page-numbers">
                                {renderPrevBtn}
                                {pageDecrementBtn}
                                {renderPageNumbers}
                                {pageIncrementBtn}
                                {renderNextBtn}
                              </ul>
                            </div>
                          ) : (
                            ""
                          )}
                        </Fragment>
                      ) : (
                        <h6>ไม่พบเกมส์ในหมวดหมู่นี้...</h6>
                      )}
                    </Fragment>
                  </div>
                </div>
                <button
                  className="btn btn-dark"
                  style={{
                    marginLeft: "60px",
                    marginBottom: "10px",
                    color: "white"
                  }}
                >
                  <i class="fas fa-angle-left"></i>{" "}
                  <Link to={`/games`} style={{ color: "white" }}>
                    {" กลับ"}
                  </Link>
                </button>
              </div>
            </div>
          </section>
        </Fragment>
      )}
    </Fragment>
  );
};

GamesCategory.propTypes = {
  getGameByCategory: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  game: state.game
});

export default connect(mapStateToProps, { getGameByCategory })(GamesCategory);
