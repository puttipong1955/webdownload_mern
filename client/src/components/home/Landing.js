import React, { Fragment, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getProgramsHit } from "../../actions/program";
import { getApplicationsHit } from "../../actions/application";
import { getGamesLast } from "../../actions/game";
import Spinner from "../layouts/Spinner";
import ItemGameHit from "./ItemGameHit";
import ItemProgramHit from "./ItemProgramHit";
import ItemApplicationHit from "./ItemApplicationHit";
import { Link } from "react-router-dom";

export const Landing = ({
  getProgramsHit,
  getApplicationsHit,
  getGamesLast,
  program: { programs_hit, loading },
  application: { applications_hit },
  game: { games_hit }
}) => {
  useEffect(
    () => {
      getProgramsHit();
      getApplicationsHit();
      getGamesLast();
    },
    [getProgramsHit],
    [getApplicationsHit],
    [getGamesLast]
  );
  return (
    <Fragment>
      {loading ? (
        <Spinner />
      ) : (
        <Fragment>
          <section className="section-head">
            <div className="container">
              <div className="row">
                <div className="col col-sm-3">
                  <div className="list-items">
                    <h1 className="label-group">โปรแกรมฟรียอดนิยม</h1>
                    <div className="list-group">
                      {programs_hit.length > 0
                        ? programs_hit.map(program => (
                            <ItemProgramHit
                              key={program.id}
                              id={program.id}
                              category={program.category}
                              text={program.text}
                              name={program.name}
                              icon={program.icon}
                            />
                          ))
                        : ""}
                    </div>
                    <Link to={"/programs"}>
                      <button className="btn-more btn btn-outline-primary">
                        เพิ่มเติม
                      </button>
                    </Link>
                  </div>
                </div>
                <div className="col col-sm-3">
                  <div className="list-items">
                    <h1 className="label-group">โปรแกรมตัวเต็มยอดนิยม</h1>
                    <div className="list-group">
                      {programs_hit.length > 0
                        ? programs_hit.map(program => (
                            <ItemProgramHit
                              key={program.id}
                              id={program.id}
                              category={program.category}
                              text={program.text}
                              name={program.name}
                              icon={program.icon}
                            />
                          ))
                        : ""}
                    </div>
                    <Link to={"/programs"}>
                      <button className="btn-more btn btn-outline-primary">
                        เพิ่มเติม
                      </button>
                    </Link>
                  </div>
                </div>
                <div className="col col-sm-3">
                  <div className="list-items">
                    <h1 className="label-group">แอพพลิเคชั่นยอดนิยม</h1>
                    <div className="list-group">
                      {applications_hit.length > 0
                        ? applications_hit.map(application => (
                            <ItemApplicationHit
                              key={application.id}
                              id={application.id}
                              category={application.category}
                              text={application.text}
                              name={application.name}
                              icon={application.icon}
                            />
                          ))
                        : ""}
                    </div>
                    <Link to={"/applications"}>
                      <button className="btn-more btn btn-outline-primary">
                        เพิ่มเติม
                      </button>
                    </Link>
                  </div>
                </div>
                <div className="col col-sm-3">
                  <div className="list-items">
                    <h1 className="label-group">เกมส์ยอดนิยม</h1>
                    <div className="list-group">
                      {games_hit.length > 0
                        ? games_hit.map(game => (
                            <ItemGameHit
                              key={game.id}
                              id={game.id}
                              text={game.info}
                              name={game.name}
                              icon={game.icon}
                            />
                          ))
                        : ""}
                    </div>
                    <Link to={"/games"}>
                      <button className="btn-more btn btn-outline-primary">
                        เพิ่มเติม
                      </button>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </Fragment>
      )}
    </Fragment>
  );
};

Landing.propTypes = {
  getProgramsHit: PropTypes.func.isRequired,
  getApplicationsHit: PropTypes.func.isRequired,
  getGamesLast: PropTypes.func.isRequired,
  program: PropTypes.object.isRequired,
  application: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  program: state.program,
  application: state.application,
  game: state.game
});

export default connect(mapStateToProps, {
  getProgramsHit,
  getApplicationsHit,
  getGamesLast
})(Landing);
