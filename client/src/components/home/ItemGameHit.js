import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const ItemGameHit = ({ id, name, text, icon }) => {
  const newText = text.replace(/[<b></b><u></u><i></u>]/g, "");
  return (
    <Link to={`/game/${id}`} className="list-group-item list-group-item-action">
      <div className="list-content">
        <div className="list-image" alt="Line">
          <img src={icon} />
        </div>
        <div className="list-text">
          <span>{name}</span>
          <p>{newText}</p>
        </div>
      </div>
    </Link>
  );
};

ItemGameHit.propTypes = {
  text: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired
};

export default ItemGameHit;
