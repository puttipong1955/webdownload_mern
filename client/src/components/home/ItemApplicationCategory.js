import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const ItemApplicationCategory = ({ label }) => {
  return (
    <Link
      to={`/applications/category/${label}`}
      class="list-group-item list-group-item-action list-group-item-light"
    >
      {label}
    </Link>
  );
};

ItemApplicationCategory.propTypes = {
  label: PropTypes.string.isRequired
};

export default ItemApplicationCategory;
