import React, { Fragment, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  getProgramsLastLimit,
  getProgramsCategories
} from "../../actions/program";
import {
  getApplicationsLastLimit,
  getApplicationsCategories
} from "../../actions/application";
import Spinner from "../layouts/Spinner";
import ItemProgramLast from "./ItemProgramLast";
import ItemAppLast from "./ItemAppLast";
import ItemProgramCategory from "./ItemProgramCategory";
import ItemApplicationCategory from "./ItemApplicationCategory";
import { Link, Redirect } from "react-router-dom";

export const Landing2 = ({
  getProgramsLastLimit,
  getApplicationsLastLimit,
  getApplicationsCategories,
  getProgramsCategories,
  program: { programs_limit, loading, pro_categories },
  application: { applications_limit, app_categories }
}) => {
  useEffect(
    () => {
      getProgramsLastLimit();
      getApplicationsLastLimit();
      getApplicationsCategories();
      getProgramsCategories();
    },
    [getProgramsLastLimit],
    [getApplicationsLastLimit],
    [getApplicationsCategories],
    [getProgramsCategories]
  );
  return (
    <Fragment>
      {loading ? (
        ""
      ) : (
        <Fragment>
          <section class="section-pro-app">
            <div class="container-lg">
              <div class="row">
                <div class="col-sm-2 cont1">
                  <h2 class="cetagories-label">หมวดหมู่โปรแกรม</h2>
                  <div class="list-group cetagories-list">
                    {pro_categories.length > 0 ? (
                      pro_categories.map(category => (
                        <ItemProgramCategory
                          key={category.category}
                          label={category.category}
                        />
                      ))
                    ) : (
                      <h4>ไม่พบประเภทโปรแกรม...</h4>
                    )}
                  </div>
                </div>
                <div class="col col-sm-4 cont2">
                  <div class="list-items program-list">
                    <div class="btn-program">
                      <h1 class="head btn">โปรแกรมใหม่ล่าสุด</h1>
                    </div>
                    <div class="program-lists list-group program-list-group">
                      {programs_limit.length > 0 ? (
                        programs_limit.map(program => (
                          <ItemProgramLast
                            key={program.id}
                            id={program.id}
                            category={program.category}
                            text={program.text}
                            name={program.name}
                            icon={program.icon}
                            date={program.create_date}
                          />
                        ))
                      ) : (
                        <h4>ไม่พบโปรแกรมล่าสุด...</h4>
                      )}
                    </div>
                  </div>
                  <div class="program-btn-more">
                    <Link to={"/programs"}>
                      <button class="btn-more btn btn-outline-primary">
                        เพิ่มเติม
                      </button>
                    </Link>
                  </div>
                </div>
                <div class="col col-sm-4 cont3">
                  <div class="list-items games-list">
                    <div class="btn-games">
                      <h1 class="head btn">แอพพลิเคชั่นใหม่ล่าสุด</h1>
                    </div>
                    <div class="list-group games-list-group">
                      {applications_limit.length > 0 ? (
                        applications_limit.map(application => (
                          <ItemAppLast
                            key={application.id}
                            id={application.id}
                            category={application.category}
                            text={application.text}
                            name={application.name}
                            icon={application.icon}
                            date={application.create_date}
                          />
                        ))
                      ) : (
                        <h4>ไม่พบโปรแกรมล่าสุด...</h4>
                      )}
                    </div>
                  </div>
                  <div class="games-btn-more">
                    <Link to={"/applications"}>
                      <button class="btn-more btn btn-outline-primary btn-programs">
                        เพิ่มเติม
                      </button>
                    </Link>
                  </div>
                </div>
                <div class="col-sm-2 cont4">
                  <h2 class="games-label">หมวดหมู่แอพพลิเคชั่น</h2>
                  <div class="list-group games-list">
                    {app_categories.length > 0 ? (
                      app_categories.map(category => (
                        <ItemApplicationCategory
                          key={category.category}
                          label={category.category}
                        />
                      ))
                    ) : (
                      <h4>ไม่พบประเภทแอพพลิเคชั่น...</h4>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </section>
        </Fragment>
      )}
    </Fragment>
  );
};

Landing2.propTypes = {
  getProgramsLastLimit: PropTypes.func.isRequired,
  getApplicationsLastLimit: PropTypes.func.isRequired,
  getApplicationsCategories: PropTypes.func.isRequired,
  getProgramsCategories: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  program: state.program,
  application: state.application
});

export default connect(mapStateToProps, {
  getProgramsLastLimit,
  getApplicationsLastLimit,
  getApplicationsCategories,
  getProgramsCategories
})(Landing2);
