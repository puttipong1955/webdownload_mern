import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Moment from "moment";

const ItemProgramLast = ({ category, id, name, text, icon, date }) => {
  let dateCreate = Moment(date)
    .utc()
    .format("YYYY-MM-DD");
  const newText = text.replace(/[<b></b><u></u><i></u>]/g, "");
  return (
    <Link
      to={`/program/${category}/${id}`}
      class="program-content list-group-item list-group-item-action"
    >
      <div class="program-list-content">
        <div class="program-list-image" alt="Line">
          <img src={icon} />
        </div>
        <div class="program-list-text">
          <span>{name}</span>
          <p>{newText}</p>
          <i class="calendar fa fa-calendar-alt" aria-hidden="false">
            <sub>{dateCreate}</sub>
          </i>
          <i class="fa fa-folder pin" aria-hidden="false">
            <Link to={`/programs/category/${category}`}>
              <sub>{category}</sub>
            </Link>
          </i>
          <button class="btn btn-success hide">></button>
        </div>
      </div>
    </Link>
  );
};

ItemProgramLast.propTypes = {
  text: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired
};

export default ItemProgramLast;
