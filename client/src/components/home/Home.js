import React, { useEffect } from "react";
import Landing from "./Landing";
import Landing2 from "./Landing2";
import Landing3 from "./Landing3";
import "../../css/style.css";

export const Home = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  });

  return (
    <div>
      <Landing />
      <Landing2 />
      <Landing3 />
    </div>
  );
};

export default Home;
