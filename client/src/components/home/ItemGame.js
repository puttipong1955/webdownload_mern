import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Moment from "moment";

const ItemGame = ({ id, name, icon, date }) => {
  let dateCreate = Moment(date)
    .utc()
    .format("YYYY-MM-DD");
  return (
    <div class="games-gard">
      <Link to={`/game/${id}`}>
        <div class="games-image" alt={name}>
          <img src={icon} />
          <i class="calendar fa fa-calendar-alt" aria-hidden="false">
            {dateCreate}
          </i>
        </div>
      </Link>
    </div>
  );
};

ItemGame.propTypes = {
  name: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired
};

export default ItemGame;
