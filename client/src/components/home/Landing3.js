import React, { Fragment, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getGamesMostLimit } from "../../actions/game";
import ItemGame from "./ItemGame";
import Spinner from "../layouts/Spinner";
import { Link } from "react-router-dom";

export const Landing3 = ({
  getGamesMostLimit,
  game: { games_most, loading }
}) => {
  useEffect(() => {
    getGamesMostLimit();
  }, [getGamesMostLimit]);
  return (
    <Fragment>
      {loading ? (
        ""
      ) : (
        <Fragment>
          <div>
            <div class="section-app">
              <div class="container-lg">
                <div class="row">
                  <div class="games-content col-12">
                    <div class="btn-program label-game">
                      <h1>เกมส์ยอดนิยม</h1>
                    </div>
                    {games_most.length > 0 ? (
                      games_most.map(game => (
                        <ItemGame
                          key={game.id}
                          id={game.id}
                          name={game.name}
                          icon={game.icon}
                          date={game.create_date}
                        />
                      ))
                    ) : (
                      <h4>ไม่พบเกมส์ล่าสุด...</h4>
                    )}
                    <div class="next-more">
                      <Link to={"/games"}>
                        <i class="far fa-arrow-alt-circle-right"></i>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Fragment>
      )}
    </Fragment>
  );
};

const mapStateToProps = state => ({
  game: state.game
});

export default connect(mapStateToProps, {
  getGamesMostLimit
})(Landing3);
