import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const ItemProgramCategory = ({ label }) => {
  return (
    <Link
      to={`/programs/category/${label}`}
      class="list-group-item list-group-item-action list-group-item-light"
    >
      {label}
    </Link>
  );
};

ItemProgramCategory.propTypes = {
  label: PropTypes.string.isRequired
};

export default ItemProgramCategory;
