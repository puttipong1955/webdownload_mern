import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const CategoryItem = ({ label }) => {
  return (
    <Link
      to={`/applications/category/${label}`}
      className="cate-list-item list-group-item list-group-item-action list-group-item-light"
    >
      {label}
    </Link>
  );
};

CategoryItem.propTypes = {
  label: PropTypes.string.isRequired
};

export default CategoryItem;
