import React from "react";
import Moment from "moment";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const ListItem = ({ id, category, name, icon, text, date }) => {
  let dateCreate = Moment(date)
    .utc()
    .format("YYYY-MM-DD");

  const newText = text.replace(/[<b></b><u></u><i></u>]/g, "");
  return (
    <Link
      to={`/application/${category}/${id}`}
      className="program-content list-group-item list-group-item-action"
    >
      <div className="program-list-content">
        <div className="program-list-image">
          <img src={icon} />
        </div>
        <div className="program-list-text">
          <span>{name}</span>
          <p>{newText}</p>
          <i className="calendar fa fa-calendar-alt" aria-hidden="false">
            <sub>{dateCreate}</sub>
          </i>
          <i class="fa fa-folder pin" aria-hidden="false">
            <Link to={`/applications/category/${category}`}>
              <sub>{category}</sub>
            </Link>
          </i>
          <button className="btn btn-success">Download</button>
        </div>
      </div>
    </Link>
  );
};

ListItem.propTypes = {
  name: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired
};

export default ListItem;
