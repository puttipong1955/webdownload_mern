import React from "react";
import PropTypes from "prop-types";
import "../../css/login.css";
import { Link } from "react-router-dom";

const AdminHome = props => {
  return (
    <div className="container">
      <div className="row" style={{ marginTop: "100px" }}>
        <div className="col-sm form">
          <Link to="/admin/programs">
            <p>Programs</p>
          </Link>
        </div>

        <div className="col-sm form">
          <Link to="/admin/applications">
            <p>Applications</p>
          </Link>
        </div>

        <div className="col-sm form">
          <Link to="/admin/games">
            <p>games</p>
          </Link>
        </div>
      </div>
    </div>
  );
};

AdminHome.propTypes = {};

export default AdminHome;
