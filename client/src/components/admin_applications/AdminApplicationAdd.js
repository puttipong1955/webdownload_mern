import React from "react";
import PropTypes from "prop-types";
import { Fragment, useState } from "react";
import "../../css/admin.css";
import { Link, Redirect } from "react-router-dom";
import { postProgram } from "../../actions/program";
import { connect } from "react-redux";

const AdminApplicationAdd = ({ postProgram }) => {
  const [name, setName] = useState("");
  const [category, setCategory] = useState("");
  const [size, setSize] = useState("");
  const [os, setOs] = useState("");
  const [copyright, setCopyright] = useState("");
  const [version, setVersion] = useState("");
  const [link, setLink] = useState("");
  const [icon, setIcon] = useState("");
  const [pic, setPic] = useState("");
  const [pic2, setPic2] = useState("");
  const [text, setText] = useState("");
  const [news, setNews] = useState("");

  const onSubmit = e => {
    e.preventDefault();

    const data = {
      name: name,
      category: category,
      text: text,
      size: size,
      os: os,
      copyright: copyright,
      version: version,
      link: link,
      icon: icon,
      pic: pic,
      pic2: pic2,
      news: news,
      application: "1",
      download: "0"
    };

    if (
      data.name.length === 0 ||
      data.category.length === 0 ||
      data.text.length === 0 ||
      data.link.length === 0 ||
      data.icon.length === 0
    ) {
      alert("กรุณกรอกชื่อ, ประเภท, รายละเอียด, ลิ้งค์, ไอคอนแอพพลิเคชั่น");
    } else {
      postProgram(data);
    }
  };

  return (
    <div>
      <Fragment>
        <section className="section-content">
          <div className="container-lg">
            <form noValidate onSubmit={onSubmit}>
              <div className="row">
                <div className="col col-md-5 c1">
                  <ul>
                    <li>
                      <Link to={"/admin/applications"}>
                        <button
                          className="btn btn-sm btn-dark btnAdd"
                          style={{
                            marginLeft: "-80px",
                            marginTop: "-80px",
                            marginBottom: "10px",
                            position: "absolute"
                          }}
                        >
                          <i class="fas fa-chevron-left"></i>
                          {" Back"}
                        </button>
                      </Link>
                    </li>
                    <li>
                      <input
                        name="setName"
                        placeholder="ชื่อ"
                        onChange={e => setName(e.target.value)}
                        value={name}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setCategory"
                        placeholder="ประเภท"
                        onChange={e => setCategory(e.target.value)}
                        value={category}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setSize"
                        placeholder="ขนาด"
                        onChange={e => setSize(e.target.value)}
                        value={size}
                      />
                    </li>
                    <li>
                      <input
                        name="setOs"
                        placeholder="ระบบปฏิบัตการ"
                        onChange={e => setOs(e.target.value)}
                        value={os}
                      />
                    </li>
                    <li>
                      <input
                        name="setCopyright"
                        placeholder="ลิขสิทธิ์"
                        onChange={e => setCopyright(e.target.value)}
                        value={copyright}
                      />
                    </li>
                    <li>
                      <input
                        name="setVersion"
                        placeholder="เวอร์ชั่น"
                        onChange={e => setVersion(e.target.value)}
                        value={version}
                      />
                    </li>
                    <li>
                      <input
                        name="setLink"
                        placeholder="ลิ้งค์ดาวน์โหลด"
                        onChange={e => setLink(e.target.value)}
                        value={link}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setIcon"
                        placeholder="ไอคอน"
                        onChange={e => setIcon(e.target.value)}
                        value={icon}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setPic"
                        placeholder="รูปภาพ 1"
                        onChange={e => setPic(e.target.value)}
                        value={pic}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setPic2"
                        placeholder="รูปภาพ 2"
                        onChange={e => setPic2(e.target.value)}
                        value={pic2}
                      />
                    </li>
                  </ul>
                </div>
                <div className="col col-md-7 c2">
                  <ul>
                    <li>
                      <textarea
                        className="text"
                        name="setText"
                        placeholder="รายละเอียด"
                        onChange={e => setText(e.target.value)}
                        value={text}
                        required
                      />
                    </li>
                    <li>
                      <textarea
                        className="news"
                        name="setNews"
                        placeholder="คุณสมบัติใหม่"
                        onChange={e => setNews(e.target.value)}
                        value={news}
                      />
                    </li>
                    <li>
                      <button
                        className="btn btn-lg btn-success btnAdd"
                        type="submit"
                      >
                        <i class="fas fa-plus"></i>
                        {" Add Application"}
                      </button>
                    </li>
                  </ul>
                </div>
              </div>
            </form>
          </div>
        </section>
      </Fragment>
    </div>
  );
};

AdminApplicationAdd.propTypes = {
  postProgram: PropTypes.func.isRequired
};

export default connect(null, { postProgram })(AdminApplicationAdd);
