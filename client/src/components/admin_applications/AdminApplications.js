import React, { Fragment, useEffect, useState } from "react";
import PropTypes from "prop-types";
import "../../css/programs.css";
import { connect } from "react-redux";
import { getApplications } from "../../actions/application";
import Spinner from "../layouts/Spinner";
import ListItem from "./ListItem";
import { Link } from "react-router-dom";

const AdminApplications = ({
  getApplications,
  application: { applications, loading }
}) => {
  const [currentPage, setCurrentPage] = useState(1);
  const [todosPerPage, setTodosPerPage] = useState(10);
  const [upperPageBound, setUpperPageBound] = useState(10);
  const [lowerPageBound, setLowerPageBound] = useState(0);
  const [isPrevBtnActive, setIsPrevBtnActive] = useState("disabled");
  const [isNextBtnActive, setIsNextBtnActive] = useState("");
  const [pageBound, setPageBound] = useState(10);

  useEffect(() => {
    window.scrollTo(0, 0);
    getApplications();
  }, [getApplications]);

  const handleClick = event => {
    window.scrollTo(0, 0);
    let listid = Number(event.target.id);
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const setPrevAndNextBtnClass = listid => {
    let totalPage = Math.ceil(applications.length / todosPerPage);
    setIsNextBtnActive("disabled");
    setIsPrevBtnActive("disabled");
    if (totalPage === listid && totalPage > 1) {
      setIsPrevBtnActive("");
    } else if (listid === 1 && totalPage > 1) {
      setIsNextBtnActive("");
    } else if (totalPage > 1) {
      setIsNextBtnActive("");
      setIsPrevBtnActive("");
    }
  };

  const btnIncrementClick = () => {
    setUpperPageBound(upperPageBound + pageBound);
    setLowerPageBound(lowerPageBound + pageBound);
    let listid = upperPageBound + 1;
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const btnDecrementClick = () => {
    setUpperPageBound(upperPageBound - pageBound);
    setLowerPageBound(lowerPageBound - pageBound);
    let listid = upperPageBound - pageBound;
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const btnPrevClick = () => {
    if ((currentPage - 1) % pageBound === 0) {
      setUpperPageBound(upperPageBound - pageBound);
      setLowerPageBound(lowerPageBound - pageBound);
    }
    let listid = currentPage - 1;
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const btnNextClick = () => {
    if (currentPage + 1 > upperPageBound) {
      setUpperPageBound(upperPageBound + pageBound);
      setLowerPageBound(lowerPageBound + pageBound);
    }
    let listid = currentPage + 1;
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const indexOfLastTodo = currentPage * todosPerPage;
  const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
  const currentTodos = applications.slice(indexOfFirstTodo, indexOfLastTodo);

  const renderTodos = currentTodos.map((todo, index) => {
    return (
      <ListItem
        key={index}
        id={todo.id}
        category={todo.category}
        name={todo.name}
        text={todo.text}
        icon={todo.icon}
        date={todo.create_date}
        download={todo.download}
        app={todo.application}
      />
    );
  });

  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(applications.length / todosPerPage); i++) {
    pageNumbers.push(i);
  }

  const renderPageNumbers = pageNumbers.map(number => {
    if (number === 1 && currentPage === 1) {
      return (
        <li key={number} className=" page-item" id={number}>
          <a id={number} onClick={handleClick} className="page-link">
            {number}
          </a>
        </li>
      );
    } else if (number < upperPageBound + 1 && number > lowerPageBound) {
      return (
        <li key={number} id={number} className="page-item">
          <a id={number} onClick={handleClick} className="page-link">
            {number}
          </a>
        </li>
      );
    }
  });
  let pageIncrementBtn = null;
  if (pageNumbers.length > upperPageBound) {
    pageIncrementBtn = (
      <li className="page-item">
        <a href="#" onClick={btnIncrementClick} className="page-link">
          {" "}
          &hellip;{" "}
        </a>
      </li>
    );
  }
  let pageDecrementBtn = null;
  if (lowerPageBound >= 1) {
    pageDecrementBtn = (
      <li className="page-item">
        <a href="#" onClick={btnDecrementClick} className="page-link">
          {" "}
          &hellip;{" "}
        </a>
      </li>
    );
  }
  let renderPrevBtn = null;
  if (isPrevBtnActive === "disabled") {
    renderPrevBtn = (
      <li className={`page-item ${isPrevBtnActive}`}>
        <a href="#" className="page-link">
          <span id="btnPrev "> {`<`}</span>
        </a>
      </li>
    );
  } else {
    renderPrevBtn = (
      <li className={`page-item ${isPrevBtnActive}`}>
        <a href="#" id="btnPrev" onClick={btnPrevClick} className="page-link">
          {" "}
          {`<`}{" "}
        </a>
      </li>
    );
  }
  let renderNextBtn = null;
  if (isNextBtnActive === "disabled") {
    renderNextBtn = (
      <li className={`page-item ${isNextBtnActive}`}>
        <a href="#" className="page-link">
          <span id="btnNext"> {`>`}</span>
        </a>
      </li>
    );
  } else {
    renderNextBtn = (
      <li className={`page-item ${isNextBtnActive}`}>
        <a href="#" id="btnNext" onClick={btnNextClick} className="page-link">
          {" "}
          {`>`}{" "}
        </a>
      </li>
    );
  }
  return (
    <div>
      <Fragment>
        <section className="section-content">
          <div className="container-lg">
            <div className="row">
              <div className="col col-md-12 cont2">
                <Link to={"/admin/application/add"}>
                  <button className="btn btn-lg btn-success btnAdd">
                    <i class="fas fa-plus"></i>
                    {" Add Application"}
                  </button>
                </Link>
                {loading ? (
                  <Spinner />
                ) : (
                  <Fragment>
                    <div className="list-items program-list">
                      <div className="list-group program-list-group">
                        {renderTodos}
                        <div className="paginationBox2">
                          <ul className="pagination" id="page-numbers">
                            {renderPrevBtn}
                            {pageDecrementBtn}
                            {renderPageNumbers}
                            {pageIncrementBtn}
                            {renderNextBtn}
                          </ul>
                        </div>
                      </div>
                    </div>
                  </Fragment>
                )}
              </div>
            </div>
          </div>
        </section>
      </Fragment>
    </div>
  );
};

AdminApplications.propTypes = {
  getApplications: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  application: state.application
});

export default connect(mapStateToProps, {
  getApplications
})(AdminApplications);
