import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { Link, Redirect } from "react-router-dom";

export const ItemRandom = ({ id, icon, text, name, category }) => {
  const newText = text.replace(/[<b></b><u></u><i></u>]/g, "");
  return (
    <Fragment>
      <div className="item program-crad">
        <div className="gard-image col">
          <Link to={`/program/${category}/${id}`}>
            <a onClick={<Redirect to={`/application/${category}/${id}`} />}>
              <img src={icon} className="img-fluid" />
              <span>{name}</span>
            </a>
          </Link>
        </div>
        <div className="gard-content col">
          <p>{newText}</p>
        </div>
      </div>
    </Fragment>
  );
};

export default ItemRandom;
