import React, { Fragment, useEffect, useState } from "react";
import PropTypes from "prop-types";
import "../../css/programs.css";
import { connect } from "react-redux";
import {
  getProgramsById,
  getProgramsCategories,
  getProgramsHit,
  putProgramDownload,
  getProgramsRamdomByCategory3
} from "../../actions/program";
import { getApplicationsHit } from "../../actions/application";
import Spinner from "../layouts/Spinner";

import CategoryItem from "./CategoryItem";
import ProgramHitItem from "./ProgramHitItem";
import ApplicationHitItem from "./ApplicationHitItem";
import Carousel from "./Carousel";
import ItemRandom from "./ItemRandom";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";

const Program = ({
  getProgramsById,
  getProgramsCategories,
  getProgramsHit,
  getApplicationsHit,
  putProgramDownload,
  getProgramsRamdomByCategory3,
  program: {
    program,
    pro_categories,
    programs_hit,
    programs_random_category,
    loading
  },
  application: { applications_hit },
  match
}) => {
  const [tags, setTags] = useState("tags");

  useEffect(
    () => {
      window.scrollTo(0, 0);
      getProgramsById(match.params.id);
      getProgramsRamdomByCategory3(match.params.category);
      getProgramsCategories();
      getProgramsHit();
      getApplicationsHit();
    },
    [getProgramsById],
    [getProgramsCategories],
    [getProgramsHit],
    [getApplicationsHit]
  );

  const putDownload = () => {
    putProgramDownload(match.params.id);
  };

  const text = { __html: program.text };
  const news = { __html: program.news };

  const explode = function() {
    if (tags === "tags") setTags(program.tag);
  };
  setTimeout(explode, 1200);

  return (
    <div>
      <Helmet>
        <meta charSet="utf-8" />
        <title>{program.name}</title>
      </Helmet>
      <Fragment>
        <section className="section-content">
          <div className="container-lg">
            <div className="row">
              <div className="col-md-2 cont1">
                <span className="label-cet">หมวดหมู่</span>
                <div className="list-group cetagories-list">
                  {pro_categories.map(category => (
                    <CategoryItem
                      key={category.category}
                      label={category.category}
                    />
                  ))}
                </div>
              </div>
              <div className="col col-md-7 program-detail">
                {loading ? (
                  <Spinner />
                ) : (
                  <Fragment>
                    <img src={program.icon} />
                    <h1>{program.name}</h1>
                    <p dangerouslySetInnerHTML={text}></p>
                    <div className="carousel col col-sm-12 carousel-phone">
                      <Carousel pic={program.pic} pic2={program.pic2} />
                    </div>
                    <div className="carousel col col-md-8 col-8 carousel-desktop">
                      <Carousel pic={program.pic} pic2={program.pic2} />
                    </div>
                    <div className="col col-md-4 col-4 download">
                      <a href={program.link} target="_blank">
                        <button
                          className="btn btn-success"
                          onClick={putDownload}
                        >
                          DOWNLOAD
                          <i className="fas fa-file-download" />
                        </button>
                      </a>
                      <div className="download-info">
                        <ul className="list-group list-group-flush">
                          <li className="list-group-item">
                            ขนาด: {program.size}
                          </li>
                          <li className="list-group-item">
                            ระบบปฏิบัติการ: {program.os}
                          </li>
                          <li className="list-group-item">
                            เวอร์ชัน: {program.version}
                          </li>
                          <li className="list-group-item">
                            สิทธิ์การใช้งาน: {program.copyright}
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="what-new">
                      <h2 className="new">คุณสมบัติใหม่ในเวอร์ชั่นนี้</h2>
                      <p dangerouslySetInnerHTML={news}></p>
                    </div>

                    <div className="tags">
                      <h3 className="tag">Tags</h3>
                      <p>
                        {tags
                          ? tags.split(",").map(tag => (
                              <Link to={`/tag/${tag.trim()}`}>
                                <i class="far fa-dot-circle fa-xs"></i>{" "}
                                {tag.trim()}{" "}
                              </Link>
                            ))
                          : "Empty Tags"}
                      </p>
                    </div>

                    <p className="prosame">โปรแกรมที่คล้ายกัน</p>
                    <div className="release-program">
                      {programs_random_category.map(pro => (
                        <ItemRandom
                          key={pro.id}
                          id={pro.id}
                          name={pro.name}
                          text={pro.text}
                          icon={pro.icon}
                          category={pro.category}
                        />
                      ))}
                    </div>
                  </Fragment>
                )}
              </div>

              <div className="col md col-3">
                <div className="list-items list-more">
                  <a href="#" className="label-group">
                    โปรแกรมที่น่าสนใจ
                  </a>
                  <div className="list-group app-list">
                    {programs_hit.length > 0 ? (
                      programs_hit.map(pro => (
                        <ProgramHitItem
                          key={pro.id}
                          id={pro.id}
                          category={pro.category}
                          name={pro.name}
                          text={pro.text}
                          icon={pro.icon}
                        />
                      ))
                    ) : (
                      <h6>ไม่พบรายการโปรแกรม...</h6>
                    )}
                  </div>
                </div>
                <div className="list-items list-more">
                  <a href="#" className="label-group">
                    แอพพลิเคชั่นที่น่าสนใจ
                  </a>
                  <div className="list-group program-list-more">
                    {applications_hit.length > 0 ? (
                      applications_hit.map(application => (
                        <ApplicationHitItem
                          key={application.id}
                          id={application.id}
                          category={application.category}
                          name={application.name}
                          text={application.text}
                          icon={application.icon}
                        />
                      ))
                    ) : (
                      <h6>ไม่พบรายการโปรแกรม...</h6>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Carousel />
      </Fragment>
    </div>
  );
};

Program.propTypes = {
  getProgramsById: PropTypes.func.isRequired,
  getProgramsCategories: PropTypes.func.isRequired,
  getProgramsHit: PropTypes.func.isRequired,
  getApplicationsHit: PropTypes.func.isRequired,
  putProgramDownload: PropTypes.func.isRequired,
  getProgramsRamdomByCategory3: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  program: state.program,
  application: state.application
});

export default connect(mapStateToProps, {
  getProgramsById,
  getProgramsCategories,
  getProgramsHit,
  getApplicationsHit,
  putProgramDownload,
  getProgramsRamdomByCategory3
})(Program);
