import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const ProgramContent = ({
  program: {
    icon,
    text,
    pic,
    pic2,
    link,
    size,
    os,
    version,
    news,
    copyright,
    name
  }
}) => {
  return (
    <Fragment>
      <img src={icon} />
      <span>{name}</span>
      <p>{text}</p>
      <div className="owl-carousel owl-theme carousel col col-md-8 col-8">
        <div className="item carousel-crad">
          <div className="gard-image col">
            <img src={pic} className="img-fluid" />
          </div>
        </div>
        <div className="item carousel-crad">
          <div className="gard-image col">
            <img src={pic2} className="img-fluid" />
          </div>
        </div>
      </div>
      <div className="col col-md-4 col-4 download">
        <Link to={`${link}`}>
          <button className="btn btn-success">
            DOWNLOAD
            <i className="fas fa-file-download" />
          </button>
        </Link>
        <div className="download-info">
          <ul className="list-group list-group-flush">
            <li className="list-group-item">ขนาด: {size}</li>
            <li className="list-group-item">ระบบปฏิบัติการ: {os}</li>
            <li className="list-group-item">เวอร์ชัน: {version}</li>
            <li className="list-group-item">สิทธิ์การใช้งาน: {copyright}</li>
          </ul>
        </div>
      </div>
      <div className="what-new">
        <p className="new">คุณสมบัติใหม่ในเวอร์ชั่นนี้</p>
        <p>{news}</p>
      </div>
    </Fragment>
  );
};

ProgramContent.propTypes = {
  program: PropTypes.array.isRequired
};

export default ProgramContent;
