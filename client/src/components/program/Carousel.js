import React, { Fragment } from "react";
import Slider from "react-slick";
import "../../css/programs.css";

const Carousel = ({ pic, pic2 }) => {
  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    pauseOnHover: true
  };

  return (
    <Fragment>
      <Slider {...settings}>
        <div className="item carousel-crad">
          <div className="gard-image col">
            <img src={pic} className="img-fluid" />
          </div>
        </div>
        <div className="item carousel-crad">
          <div className="gard-image col">
            <img src={pic2} className="img-fluid" />
          </div>
        </div>
      </Slider>
    </Fragment>
  );
};

export default Carousel;
