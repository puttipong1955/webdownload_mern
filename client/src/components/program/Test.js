import React, { useEffect, Fragment } from "react";
import { connect } from "react-redux";
import { getProgramsById } from "../../actions/program";
import PropTypes from "prop-types";

export const Test = ({ getProgramsById, id, program: { program } }) => {
  useEffect(() => {
    getProgramsById(id);
  }, [getProgramsById]);

  return (
    <div className="col col-md-7 program-detail">
      <img src="https://images.sftcdn.net/images/t_app-logo-l,f_auto,dpr_auto/p/ab2e1c88-96d0-11e6-b3fe-00163ec9f5fa/996254119/adobe-photoshop-cs6-update-photo.jpg" />
      <span>Adobe Photoshop CS6 update for Windows</span>
      <p>
        Photoshop เป็นชุดเครื่องมือแก้ไขภาพชั้นนำของอุตสาหกรรมมาหลายปีแล้ว
        หากคุณสนใจในการตรวจสอบ Photoshop
        รุ่นเก่าบางรุ่นทำไมไม่ลองดูบทแนะนำของเราเกี่ยวกับ Photoshop 7.0 ,
        Photoshop CS5 และ Photoshop CS6
        <a href>Adobe Photoshop CS6 update</a>{" "}
        เป็นแพทช์ฟรีอย่างเป็นทางการสำหรับอัพเดตซอฟต์แวร์แก้ไขภาพที่มีชื่อเสียงเป็นเวอร์ชัน
        13.0.1.1 ตลอดจนแก้ไขข้อผิดพลาดต่างๆ โปรดทราบว่า คุณต้องติดตั้ง Photoshop
        ไว้เพื่อใช้งานโปรแกรมอัพเดทนี้ (และเวอร์ชั่นที่ถูกต้อง) -
        ถ้าคุณไม่ได้ติดตั้งโปรแกรมนี้ มันจะไม่มีประโยชน์ Photoshop
        เป็นชุดเครื่องมือแก้ไขภาพชั้นนำของอุตสาหกรรมมาหลายปีแล้ว
        หากคุณสนใจในการตรวจสอบ Photoshop
        รุ่นเก่าบางรุ่นทำไมไม่ลองดูบทแนะนำของเราเกี่ยวกับ Photoshop 7.0 ,
        Photoshop CS5 และ Photoshop CS6
        <b>Adobe Photoshop CS6 update</b>{" "}
        เป็นแพทช์ฟรีอย่างเป็นทางการสำหรับอัพเดตซอฟต์แวร์แก้ไขภาพที่มีชื่อเสียงเป็นเวอร์ชัน
        13.0.1.1 ตลอดจนแก้ไขข้อผิดพลาดต่างๆ โปรดทราบว่า คุณต้องติดตั้ง Photoshop
        ไว้เพื่อใช้งานโปรแกรมอัพเดทนี้ (และเวอร์ชั่นที่ถูกต้อง) -
        ถ้าคุณไม่ได้ติดตั้งโปรแกรมนี้ มันจะไม่มีประโยชน์
      </p>
      <div className="owl-carousel owl-theme carousel col col-md-8 col-8">
        <div className="item carousel-crad">
          <div className="gard-image col">
            <img
              src="https://images.sftcdn.net/images/t_app-cover-l,f_auto/p/ab2e1c88-96d0-11e6-b3fe-00163ec9f5fa/2009690119/adobe-photoshop-cs6-update-CS6.png"
              className="img-fluid"
              alt={12}
            />
          </div>
        </div>
        <div className="item carousel-crad">
          <div className="gard-image col">
            <img
              src="https://images.sftcdn.net/images/t_app-cover-l,f_auto/p/ab2e1c88-96d0-11e6-b3fe-00163ec9f5fa/3082761092/adobe-photoshop-cs6-update-sSpJ.png"
              className="img-fluid"
              alt={12}
            />
          </div>
        </div>
      </div>
      <div className="col col-md-4 col-4 download">
        <a href>
          <button className="btn btn-success">
            DOWNLOAD
            <i className="fas fa-file-download" />
          </button>
        </a>
        <div className="download-info">
          <ul className="list-group list-group-flush">
            <li className="list-group-item">ขนาด: 10MB</li>
            <li className="list-group-item">ระบบปฏิบัติการ: Windows 7/8/10</li>
            <li className="list-group-item">เวอร์ชัน: 13.0.6</li>
            <li className="list-group-item">สิทธิ์การใช้งาน: ฟรี</li>
          </ul>
        </div>
      </div>
      <div className="what-new">
        <p className="new">คุณสมบัติใหม่ในเวอร์ชั่นนี้</p>
        <p>
          – Improved video recognition in web players
          <br />
          – Fixed problems with downloading for several types of video streams
          <br />
          – Fixed bugs
          <br />
          – Improved video recognition in web players
          <br />
          – Fixed problems with downloading for several types of video streams
          <br />
          – Fixed bugs
          <br />
          – Fixed bugs
          <br />
          – Fixed bugs
          <br />
        </p>
      </div>
      <sub>โปรแกรมที่คล้ายกัน</sub>
      <div className="release-program">
        <div className="item program-crad">
          <div className="gard-image col">
            <a href="#">
              <img
                src="https://software.thaiware.com/upload_misc/software/2017_12/thumbnails/13631_171229081427JS.png"
                className="img-fluid"
                alt={12}
              />
              <span>
                Guru Gooroo (App Guru Gooroo เรียนออนไลน์ ติวออนไลน์ บน Android
                และ iOS)
              </span>
            </a>
          </div>
          <div className="gard-content col">
            <p>
              ดาวน์โหลดแอป Guru Gooroo หรือภาษาไทย กูรู กูรู้
              แพลตฟอร์มที่ใช้ในการเรียนรู้แบบ ไลฟ์วีดีโอ กับติวเตอร์ หรือ
              ผู้สอนหรือกูรูในเรื่องต่างๆ ใน สาขาวิชาต่างๆ หรือจะ สาขาทั่วๆ ไป
              ก็ได้
            </p>
          </div>
        </div>
        <div className="item program-crad">
          <div className="gard-image col">
            <a href="#">
              <img
                src="https://108download.com/wp-content/uploads/Calibre-150x150.png"
                className="img-fluid"
                alt={12}
              />
              <span>Calibre 4.5.0 (64-bit)</span>
            </a>
          </div>
          <div className="gard-content col">
            <p>
              Calibre เป็นโปรแกรมสำหรับจัดการข้อมูลหนังสือออนไลน์ E-Book ต่างๆ
              ที่จะช่วยให้ผู้ใช้งานสามารถใช้งานได้ง่ายขึ้น สะดวก
              รวดเร็วในการเลือกอ่านหนังสือ หรือ เปิดใช้งานอ่านหนังสือ E-Book
              สามารถจัดการหนังสือทั
            </p>
          </div>
        </div>
        <div className="item program-crad">
          <div className="gard-image col">
            <a href="#">
              <img
                src="https://108download.com/wp-content/uploads/virtualdj-150x150.png"
                className="img-fluid"
                alt={12}
              />
              <span>Virtual DJ 2020 Build 5451</span>
            </a>
          </div>
          <div className="gard-content col">
            <p>
              VirtualDJ (VDJ) หนึ่งในโปรแกรมที่ใช้สำหรับในการจัดเสียงเพลง
              โดยโปรแกรมตัวนี้ถูกพัฒนาขึ้นมาเพื่อช่วยให้ผู้ใช้งานสามารถมิกซ์เสียง
              ปรับแต่งเสียงได้ตามต้องการ โดยใช้งานกับการเล่นสื่อดิจิตอลต่างๆ
              เช่น ออดิ
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

Test.propTypes = {
  getProgramsById: PropTypes.func.isRequired,
  program: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  program: state.program
});

export default connect(mapStateToProps, { getProgramsById })(Test);
