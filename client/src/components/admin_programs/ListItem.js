import React, { Fragment } from "react";
import Moment from "moment";
import PropTypes from "prop-types";
import { Link, Redirect } from "react-router-dom";
import { deleteProgram } from "../../actions/program";
import { connect } from "react-redux";
import history from "../../history";

const ListItem = ({
  deleteProgram,
  id,
  category,
  name,
  icon,
  text,
  date,
  app,
  download
}) => {
  let dateCreate = Moment(date)
    .utc()
    .format("YYYY-MM-DD");
  const newText = text.replace(/[<b></b><u></u><i></u>]/g, "");

  const onDelete = () => {
    if (window.confirm("คุณต้องการลบหรือไม่?") === true) {
      deleteProgram(id);
      window.location.reload();
    }
  };

  return (
    <Fragment>
      <div className="row">
        <div className="col col-md-2">
          <div className="viewdownload">
            <span>{download}</span>
            <p>Views</p>
          </div>
        </div>
        <div className="col col-md-8">
          <Link to={`/program/${category}/${id}`}>
            <button
              class="btn btn-outline-info btn-sm"
              style={{
                position: "absolute",
                marginLeft: "-40px",
                marginTop: "88px"
              }}
            >
              View
            </button>
          </Link>

          <Link
            to={`/admin/program/${id}`}
            className="program-content list-group-item list-group-item-action"
          >
            <div className="program-list-content">
              <div className="program-list-image">
                <img src={icon} />
              </div>
              <div
                className="program-list-text"
                style={{ marginLeft: "-50px" }}
              >
                <span>{name}</span>
                <p>{newText}</p>
                <i className="calendar fa fa-calendar-alt" aria-hidden="false">
                  <sub>{dateCreate}</sub>
                </i>
              </div>
            </div>
          </Link>
        </div>
        <div className="col col-md-2 btnDelete">
          <button onClick={onDelete} className="btn btn-danger btn-sm">
            Delete
          </button>
        </div>
      </div>
    </Fragment>
  );
};

ListItem.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  deleteProgram: PropTypes.func.isRequired
};

export default connect(null, { deleteProgram })(ListItem);
