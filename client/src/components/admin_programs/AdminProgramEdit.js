import React from "react";
import PropTypes from "prop-types";
import { Fragment, useState, useEffect, useRef } from "react";
import "../../css/admin.css";
import { Link } from "react-router-dom";
import { putProgram, getProgramsById } from "../../actions/program";
import { connect } from "react-redux";

const AdminProgramEdit = ({
  putProgram,
  getProgramsById,
  program: { program, loading },
  match
}) => {
  const [name, setName] = useState("name");
  const [category, setCategory] = useState("category");
  const [size, setSize] = useState("size");
  const [os, setOs] = useState("os");
  const [copyright, setCopyright] = useState("copyright");
  const [version, setVersion] = useState("version");
  const [link, setLink] = useState("link");
  const [icon, setIcon] = useState("icon");
  const [pic, setPic] = useState("pic");
  const [pic2, setPic2] = useState("pic2");
  const [text, setText] = useState("text");
  const [news, setNews] = useState("news");

  useEffect(() => {
    getProgramsById(match.params.id);
  }, [getProgramsById]);

  const explode = function() {
    if (name === "name") setName(program.name);
    if (category === "category") setCategory(program.category);
    if (size === "size") setSize(program.size);
    if (os === "os") setOs(program.os);
    if (copyright === "copyright") setCopyright(program.copyright);
    if (version === "version") setVersion(program.version);
    if (link === "link") setLink(program.link);
    if (icon === "icon") setIcon(program.icon);
    if (pic === "pic") setPic(program.pic);
    if (pic2 === "pic2") setPic2(program.pic2);
    if (text === "text") setText(program.text);
    if (news === "news") setNews(program.news);
  };
  setTimeout(explode, 2000);

  const onSubmit = e => {
    e.preventDefault();

    const data = {
      name: name,
      category: category,
      text: text,
      size: size,
      os: os,
      copyright: copyright,
      version: version,
      link: link,
      icon: icon,
      pic: pic,
      pic2: pic2,
      news: news,
      application: "0"
    };

    if (
      data.name.length === 0 ||
      data.category.length === 0 ||
      data.text.length === 0 ||
      data.link.length === 0 ||
      data.icon.length === 0
    ) {
      alert("กรุณกรอกชื่อ, ประเภท, รายละเอียด, ลิ้งค์, ไอคอนโปรแกรม");
    } else {
      putProgram(match.params.id, data);
    }
  };

  return (
    <div>
      <Fragment>
        <section className="section-content">
          <div className="container-lg">
            <form noValidate onSubmit={onSubmit}>
              <div className="row">
                <div className="col col-md-5 c1">
                  <ul>
                    <li>
                      <Link to={"/admin/programs"}>
                        <button
                          className="btn btn-sm btn-dark btnAdd"
                          style={{
                            marginLeft: "-80px",
                            marginTop: "-80px",
                            marginBottom: "10px",
                            position: "absolute"
                          }}
                        >
                          <i class="fas fa-chevron-left"></i>
                          {" Back"}
                        </button>
                      </Link>
                    </li>
                    <li>
                      <input
                        name="setName"
                        placeholder="ชื่อ"
                        onChange={e => setName(e.target.value)}
                        value={name}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setCategory"
                        placeholder="ประเภท"
                        onChange={e => setCategory(e.target.value)}
                        value={category}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setSize"
                        placeholder="ขนาด"
                        onChange={e => setSize(e.target.value)}
                        value={size}
                      />
                    </li>
                    <li>
                      <input
                        name="setOs"
                        placeholder="ระบบปฏิบัตการ"
                        onChange={e => setOs(e.target.value)}
                        value={os}
                      />
                    </li>
                    <li>
                      <input
                        name="setCopyright"
                        placeholder="ลิขสิทธิ์"
                        onChange={e => setCopyright(e.target.value)}
                        value={copyright}
                      />
                    </li>
                    <li>
                      <input
                        name="setVersion"
                        placeholder="เวอร์ชั่น"
                        onChange={e => setVersion(e.target.value)}
                        value={version}
                      />
                    </li>
                    <li>
                      <input
                        name="setLink"
                        placeholder="ลิ้งค์ดาวน์โหลด"
                        onChange={e => setLink(e.target.value)}
                        value={link}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setIcon"
                        placeholder="ไอคอน"
                        onChange={e => setIcon(e.target.value)}
                        value={icon}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setPic"
                        placeholder="รูปภาพ 1"
                        onChange={e => setPic(e.target.value)}
                        value={pic}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setPic2"
                        placeholder="รูปภาพ 2"
                        onChange={e => setPic2(e.target.value)}
                        value={pic2}
                      />
                    </li>
                  </ul>
                </div>
                <div className="col col-md-7 c2">
                  <ul>
                    <li>
                      <textarea
                        className="text"
                        name="setText"
                        placeholder="รายละเอียด"
                        onChange={e => setText(e.target.value)}
                        value={text}
                        required
                      />
                    </li>
                    <li>
                      <textarea
                        className="news"
                        name="setNews"
                        placeholder="คุณสมบัติใหม่"
                        onChange={e => setNews(e.target.value)}
                        value={news}
                      />
                    </li>
                    <li>
                      <button
                        className="btn btn-lg btn-success btnAdd"
                        type="submit"
                      >
                        <i class="fas fa-angle-up"></i>
                        {" Edit Program"}
                      </button>
                    </li>
                  </ul>
                </div>
              </div>
            </form>
          </div>
        </section>
      </Fragment>
    </div>
  );
};

AdminProgramEdit.propTypes = {
  putProgram: PropTypes.func.isRequired,
  getProgramsById: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  program: state.program
});

export default connect(mapStateToProps, { putProgram, getProgramsById })(
  AdminProgramEdit
);
