import React from "react";
import { Link } from "react-router-dom";
import avengersIcon from "../../asset/icon/avengers.png";
import swordIcon from "../../asset/icon/sword.png";
import simulatorIcon from "../../asset/icon/simulation.png";
import sportIcon from "../../asset/icon/football.png";
import ghostIcon from "../../asset/icon/ghost.png";
import puzzleIcon from "../../asset/icon/puzzle.png";
import managementIcon from "../../asset/icon/project-management.png";
import targetIcon from "../../asset/icon/target.png";
import "../../css/games.css";

const GameCategories = () => {
  return (
    <div className="list-items list-more ">
      <div className="list-group list-group-game ">
        <Link
          to={`/games/category/เกมส์ผจญภัย`}
          className="list-cate-content list-group-item list-group-item-action"
        >
          <div className="list-content">
            <div className="list-image col">
              <img src={avengersIcon} />
            </div>
            <div className="list-text col">
              <span>เกมส์ผจญภัย</span>
            </div>
          </div>
        </Link>
        <Link
          to={`/games/category/เกมส์ต่อสู้`}
          className="list-cate-content list-group-item list-group-item-action"
        >
          <div className="list-content">
            <div className="list-image col">
              <img src={swordIcon} />
            </div>
            <div className="list-text col">
              <span>เกมส์ต่อสู้</span>
            </div>
          </div>
        </Link>
        <Link
          to={`/games/category/เกมส์จำลองสถานการณ์จริง`}
          className="list-cate-content list-group-item list-group-item-action"
        >
          <div className="list-content">
            <div className="list-image col">
              <img src={simulatorIcon} />
            </div>
            <div className="list-text col">
              <span>เกมส์จำลองสถานการณ์จริง</span>
            </div>
          </div>
        </Link>
        <Link
          to={`/games/category/เกมส์กีฬา`}
          className="list-cate-content list-group-item list-group-item-action"
        >
          <div className="list-content">
            <div className="list-image col">
              <img src={sportIcon} />
            </div>
            <div className="list-text col">
              <span>เกมส์กีฬา แข่งรถ</span>
            </div>
          </div>
        </Link>
        <Link
          to={`/games/category/เกมส์ผี`}
          className="list-cate-content list-group-item list-group-item-action"
        >
          <div className="list-content">
            <div className="list-image col">
              <img src={ghostIcon} />
            </div>
            <div className="list-text col">
              <span>เกมส์ผี หลอน</span>
            </div>
          </div>
        </Link>
        <Link
          to={`/games/category/เกมส์เบาสมอง`}
          className="list-cate-content list-group-item list-group-item-action"
        >
          <div className="list-content">
            <div className="list-image col">
              <img src={puzzleIcon} />
            </div>
            <div className="list-text col">
              <span>เกมส์เบาสมอง</span>
            </div>
          </div>
        </Link>
        <Link
          to={`/games/category/เกมส์วางแผน`}
          className="list-cate-content list-group-item list-group-item-action"
        >
          <div className="list-content">
            <div className="list-image col">
              <img src={managementIcon} />
            </div>
            <div className="list-text col">
              <span>เกมส์วางแผน</span>
            </div>
          </div>
        </Link>
        <Link
          to={`/games/category/เกมส์ยิง`}
          className="list-cate-content list-group-item list-group-item-action"
        >
          <div className="list-content">
            <div className="list-image col">
              <img src={targetIcon} />
            </div>
            <div className="list-text col">
              <span>เกมส์ยิง สงคราม</span>
            </div>
          </div>
        </Link>
      </div>
    </div>
  );
};

export default GameCategories;
