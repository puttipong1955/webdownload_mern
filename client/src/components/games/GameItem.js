import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Moment from "moment";

const GameItem = ({ id, name, date, icon }) => {
  let dateCreate = Moment(date)
    .utc()
    .format("YYYY-MM-DD");
  return (
    <div class="games-gard">
      <Link to={`/game/${id}`}>
        <div class="games-image" alt="">
          <img src={icon} />
          <i class="calendar fa fa-calendar-alt" aria-hidden="false">
  {" "}{dateCreate}
          </i>
          <p>{name}</p>
        </div>
      </Link>
    </div>
  );
};

GameItem.propTypes = {
  icon: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
};

export default GameItem;
