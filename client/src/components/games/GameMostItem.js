import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Moment from "moment";

export const GameMostItem = ({ id, icon, date }) => {
  let dateCreate = Moment(date)
    .utc()
    .format("YYYY-MM-DD");
  return (
    <div className="games-gard">
      <Link to={`/game/${id}`}>
        <div className="games-image" alt="">
          <img src={icon} />
          <i className="calendar fa fa-calendar-alt" aria-hidden="false">{" "}
            {dateCreate}
          </i>
        </div>
      </Link>
    </div>
  );
};

GameMostItem.propTypes = {
  id: PropTypes.number.isRequired,
  icon: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired
};

export default GameMostItem;
