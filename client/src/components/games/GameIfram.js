import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Moment from "moment";

const GameIfram = ({ id, video, name, date, icon }) => {
  let dateCreate = Moment(date)
    .utc()
    .format("YYYY-MM-DD");
  return (
    <Fragment>
      <iframe
        width="100%"
        height={315}
        src={`https://www.youtube.com/embed/${video}?rel=0&autoplay=1&mute=1`}
      ></iframe>
      <div className="games-gard">
        <Link to={`/game/${id}`}>
          <div className="games-image" alt="">
            <img src={icon} />
            <i className="calendar fa fa-calendar-alt" aria-hidden="false">
              {dateCreate}
            </i>
            <p>{name}</p>
          </div>
        </Link>
      </div>
    </Fragment>
  );
};

GameIfram.propTypes = {
  icon: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  video: PropTypes.string.isRequired
};

export default GameIfram;
