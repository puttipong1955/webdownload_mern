import React, { Fragment, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { getGamesMostLimit, getGames } from "../../actions/game";
import "../../css/games.css";
import GameMostItem from "./GameMostItem";
import Spinner from "../layouts/Spinner";
import GameIfram from "./GameIfram";
import GameCategories from "./GameCategories";
import GameItem from "./GameItem";

const Games = ({
  getGamesMostLimit,
  getGames,
  game: { games, games_most, loading }
}) => {
  const [currentPage, setCurrentPage] = useState(1);
  const [todosPerPage, setTodosPerPage] = useState(18);
  const [upperPageBound, setUpperPageBound] = useState(10);
  const [lowerPageBound, setLowerPageBound] = useState(0);
  const [isPrevBtnActive, setIsPrevBtnActive] = useState("disabled");
  const [isNextBtnActive, setIsNextBtnActive] = useState("");
  const [pageBound, setPageBound] = useState(10);

  useEffect(
    () => {
      window.scrollTo(0, 0);
      getGamesMostLimit();
      getGames();
    },
    [getGamesMostLimit],
    [getGames]
  );

  const handleClick = event => {
    window.scrollTo(0, 0);
    let listid = Number(event.target.id);
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const setPrevAndNextBtnClass = listid => {
    let totalPage = Math.ceil(games.length / todosPerPage);
    setIsNextBtnActive("disabled");
    setIsPrevBtnActive("disabled");
    if (totalPage === listid && totalPage > 1) {
      setIsPrevBtnActive("");
    } else if (listid === 1 && totalPage > 1) {
      setIsNextBtnActive("");
    } else if (totalPage > 1) {
      setIsNextBtnActive("");
      setIsPrevBtnActive("");
    }
  };

  const btnIncrementClick = () => {
    setUpperPageBound(upperPageBound + pageBound);
    setLowerPageBound(lowerPageBound + pageBound);
    let listid = upperPageBound + 1;
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const btnDecrementClick = () => {
    setUpperPageBound(upperPageBound - pageBound);
    setLowerPageBound(lowerPageBound - pageBound);
    let listid = upperPageBound - pageBound;
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const btnPrevClick = () => {
    if ((currentPage - 1) % pageBound === 0) {
      setUpperPageBound(upperPageBound - pageBound);
      setLowerPageBound(lowerPageBound - pageBound);
    }
    let listid = currentPage - 1;
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const btnNextClick = () => {
    if (currentPage + 1 > upperPageBound) {
      setUpperPageBound(upperPageBound + pageBound);
      setLowerPageBound(lowerPageBound + pageBound);
    }
    let listid = currentPage + 1;
    setCurrentPage(listid);
    setPrevAndNextBtnClass(listid);
  };

  const indexOfLastTodo = currentPage * todosPerPage;
  const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
  const currentTodos = games.slice(indexOfFirstTodo, indexOfLastTodo);

  const renderTodos = currentTodos.map((todo, index) => {
    return (
      <GameItem
        key={index}
        id={todo.id}
        icon={todo.icon}
        date={todo.date}
        name={todo.name}
      />
    );
  });

  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(games.length / todosPerPage); i++) {
    pageNumbers.push(i);
  }

  const renderPageNumbers = pageNumbers.map(number => {
    if (number === 1 && currentPage === 1) {
      return (
        <li key={number} className=" page-item" id={number}>
          <a id={number} onClick={handleClick} className="page-link">
            {number}
          </a>
        </li>
      );
    } else if (number < upperPageBound + 1 && number > lowerPageBound) {
      return (
        <li key={number} id={number} className="page-item">
          <a id={number} onClick={handleClick} className="page-link">
            {number}
          </a>
        </li>
      );
    }
  });
  let pageIncrementBtn = null;
  if (pageNumbers.length > upperPageBound) {
    pageIncrementBtn = (
      <li className="page-item">
        <a href="#" onClick={btnIncrementClick} className="page-link">
          {" "}
          &hellip;{" "}
        </a>
      </li>
    );
  }
  let pageDecrementBtn = null;
  if (lowerPageBound >= 1) {
    pageDecrementBtn = (
      <li className="page-item">
        <a href="#" onClick={btnDecrementClick} className="page-link">
          {" "}
          &hellip;{" "}
        </a>
      </li>
    );
  }
  let renderPrevBtn = null;
  if (isPrevBtnActive === "disabled") {
    renderPrevBtn = (
      <li className={`page-item ${isPrevBtnActive}`}>
        <a className="page-link">
          <span id="btnPrev "> {`<`}</span>
        </a>
      </li>
    );
  } else {
    renderPrevBtn = (
      <li className={`page-item ${isPrevBtnActive}`}>
        <a href="#" id="btnPrev" onClick={btnPrevClick} className="page-link">
          {" "}
          {`<`}{" "}
        </a>
      </li>
    );
  }
  let renderNextBtn = null;
  if (isNextBtnActive === "disabled") {
    renderNextBtn = (
      <li className={`page-item ${isNextBtnActive}`}>
        <a className="page-link">
          <span id="btnNext"> {`>`}</span>
        </a>
      </li>
    );
  } else {
    renderNextBtn = (
      <li className={`page-item ${isNextBtnActive}`}>
        <a href="#" id="btnNext" onClick={btnNextClick} className="page-link">
          {" "}
          {`>`}{" "}
        </a>
      </li>
    );
  }

  return (
    <Fragment>
      <section className="section-games-hit">
        <div className="container-lg">
          <div className="row">
            <h1 className="hit-label">เกมส์ยอดนิยม</h1>
            <div className="games-hit-content col-8">
              <Fragment>
                {loading ? (
                  <Spinner />
                ) : (
                  <Fragment>
                    {games_most.length > 0 ? (
                      games_most.map(game => (
                        <GameMostItem
                          key={game.id}
                          id={game.id}
                          icon={game.icon}
                          date={game.date}
                        />
                      ))
                    ) : (
                      <h6>...</h6>
                    )}
                  </Fragment>
                )}
              </Fragment>
            </div>
            <div className="side-bar col-4">
              {games_most.slice(0, 1).map(game => (
                <GameIfram
                  key={game.id}
                  id={game.id}
                  icon={game.icon}
                  date={game.date}
                  video={game.video}
                  name={game.name}
                />
              ))}
              <GameCategories />
            </div>
          </div>
        </div>
      </section>
      <section class="section-most">
        <div class="container-lg">
          <div class="row">
            <h1 class="game-label">เกมส์ใหม่ ล่าสุด</h1>
            <div class="games-content col-12">
              <div>
                <Fragment>
                  {renderTodos}
                  <div className="paginationBox">
                    <ul className="pagination" id="page-numbers">
                      {renderPrevBtn}
                      {pageDecrementBtn}
                      {renderPageNumbers}
                      {pageIncrementBtn}
                      {renderNextBtn}
                    </ul>
                  </div>
                </Fragment>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Fragment>
  );
};

Games.propTypes = {
  getGamesMostLimit: PropTypes.func.isRequired,
  getGames: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  game: state.game
});

export default connect(mapStateToProps, { getGamesMostLimit, getGames })(Games);
