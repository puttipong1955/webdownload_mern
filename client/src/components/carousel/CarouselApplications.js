import React, { Fragment, useEffect } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { getApplicationRandom10 } from "../../actions/application";
import { connect } from "react-redux";
import Slider from "react-slick";
import "../../css/programs.css";
import Loadbar from '../layouts/Loadbar'

const Carousel = ({
  getApplicationRandom10,
  application: { applications_random }
}) => {
  useEffect(() => {
    getApplicationRandom10();
  }, [getApplicationRandom10]);

  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2500,
    pauseOnHover: true
  };

  const newText = text => text.replace(/[<b></b><u></u><i></u>]/g, "");

  return (
    <Fragment>
      <section className="section-slid">
        <div className="container-lg">
          <div className="row">
            <div className="slid col-12">
              {/* carousel -------------------------------------------- */}
              <Slider {...settings}>
                {applications_random.length > 0 ? (
                  applications_random.map(applicaion => (
                    <div>
                      <div className="item carousel-crad">
                        <div className="gard-image col">
                          <a
                            href={`/application/${applicaion.category}/${applicaion.id}`}
                          >
                            <img
                              src={applicaion.icon}
                              className="img-fluid"
                              alt={applicaion.name}
                            />
                            <span>{applicaion.name}</span>
                          </a>
                        </div>
                        <div className="gard-content col">
                          <p>{newText(applicaion.text)}</p>
                        </div>
                      </div>
                    </div>
                  ))
                ) : (
                  <Loadbar/>
                )}
              </Slider>
            </div>
          </div>
        </div>
      </section>
    </Fragment>
  );
};

Carousel.propTypes = {
  getApplicationRandom10: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
  application: state.application
});

export default connect(mapStateToProps, { getApplicationRandom10 })(Carousel);
