import React, { Fragment, useEffect } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { getProgramsRamdom10 } from "../../actions/program";
import { connect } from "react-redux";
import Slider from "react-slick";
import "../../css/programs.css";
import Loadbar from '../layouts/Loadbar'

const Carousel = ({ getProgramsRamdom10, program: { programs_random } }) => {
  useEffect(() => {
    getProgramsRamdom10();
  }, [getProgramsRamdom10]);

  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2500,
    pauseOnHover: true
  };

  const newText = text => text.replace(/[<b></b><u></u><i></u>]/g, "");

  return (
    <Fragment>
      <section className="section-slid">
        <div className="container-lg">
          <div className="row">
            <div className="slid col-12">
              {/* carousel -------------------------------------------- */}
              <Slider {...settings}>
                {programs_random.length > 0 ? (
                  programs_random.map(program => (
                    <div>
                      <div className="item carousel-crad">
                        <div className="gard-image col">
                          <a
                            href={`/program/${program.category}/${program.id}`}
                          >
                            <img
                              src={program.icon}
                              className="img-fluid"
                              alt={program.name}
                            />
                            <span>{program.name}</span>
                          </a>
                        </div>
                        <div className="gard-content col">
                          <p>{newText(program.text)}</p>
                        </div>
                      </div>
                    </div>
                  ))
                ) : (
                  <Loadbar/>
                )}
              </Slider>
            </div>
          </div>
        </div>
      </section>
    </Fragment>
  );
};

Carousel.propTypes = {
  getProgramsRamdom10: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
  program: state.program
});

export default connect(mapStateToProps, { getProgramsRamdom10 })(Carousel);
