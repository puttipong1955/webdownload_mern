import React, { Fragment, useEffect } from "react";
import { Link } from "react-router-dom";

const ItemLink = ({ id, id_game, link, type, num }) => {
  const onDel = idLink => {
    window.location.reload();
  };

  return (
    <Fragment>
      <li>
        <input value={link} />
        <input className="inputNum" value={num} />
        <select className="selcetType">
          {type === 1 ? (
            <Fragment>
              <option value="1">ไฟล์เดียว</option>
              <option value="2">แบ่งพาร์ท</option>
              <option value="3">Crack</option>
              <option value="4">Patch</option>
            </Fragment>
          ) : type === 2 ? (
            <Fragment>
              <option value="2">แบ่งพาร์ท</option>
              <option value="1">ไฟล์เดียว</option>
              <option value="3">Crack</option>
              <option value="4">Patch</option>
            </Fragment>
          ) : type === 3 ? (
            <Fragment>
              <option value="3">Crack</option>
              <option value="1">ไฟล์เดียว</option>
              <option value="2">แบ่งพาร์ท</option>
              <option value="4">Patch</option>
            </Fragment>
          ) : (
            <Fragment>
              <option value="4">Patch</option>
              <option value="1">ไฟล์เดียว</option>
              <option value="2">แบ่งพาร์ท</option>
              <option value="3">Crack</option>
            </Fragment>
          )}
        </select>
        <Link to={`/admin/game/link/del/${id_game}/${id}`}>
          <button type="submit" className="btn btn-sm btn-danger btnDel">
            X
          </button>
        </Link>
      </li>
    </Fragment>
  );
};

export default ItemLink;
