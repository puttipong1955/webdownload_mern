import React from "react";
import PropTypes from "prop-types";
import { Fragment, useState } from "react";
import "../../css/admin.css";
import { Link, Redirect } from "react-router-dom";
import { postGame } from "../../actions/game";
import { connect } from "react-redux";

const AdminGameAdd = ({ postGame }) => {
  const [name, setName] = useState("");
  const [categorie, setCategorie] = useState("");
  const [size, setSize] = useState("");
  const [os, setOs] = useState("");
  const [company, setCompany] = useState("");
  const [date_out, setDate_out] = useState("");
  const [cpu, setCpu] = useState("");
  const [ram, setRam] = useState("");
  const [grafic, setGrafic] = useState("");
  const [disk, setDisk] = useState("");
  const [video, setVideo] = useState("");
  const [icon, setIcon] = useState("");
  const [pic, setPic] = useState("");
  const [pic2, setPic2] = useState("");
  const [info, setInfo] = useState("");
  const [pic3, setPic3] = useState("");
  const [pic4, setPic4] = useState("");

  const onSubmit = e => {
    e.preventDefault();

    const data = {
      name: name,
      categorie: categorie,
      info: info,
      company: company,
      date_out: date_out,
      size: size,
      os: os,
      cpu: cpu,
      ram: ram,
      grafic: grafic,
      disk: disk,
      video: video,
      icon: icon,
      pic: pic,
      pic2: pic2,
      pic3: pic3,
      pic4: pic4
    };

    if (
      data.name.length === 0 ||
      data.categorie.length === 0 ||
      data.info.length === 0 ||
      data.video.length === 0 ||
      data.icon.length === 0
    ) {
      alert("กรุณกรอกชื่อ, ประเภท, รายละเอียด, วิดีโอ, ไอคอนเกมส์");
    } else {
      postGame(data);
    }
  };

  return (
    <div>
      <Fragment>
        <section className="section-content">
          <div className="container-lg">
            <form noValidate onSubmit={onSubmit}>
              <div className="row">
                <div className="col col-md-5 c1">
                  <ul>
                    <li>
                      <Link to={"/admin/games"}>
                        <button
                          className="btn btn-sm btn-dark btnAdd"
                          style={{
                            marginLeft: "-80px",
                            marginTop: "-80px",
                            marginBottom: "10px",
                            position: "absolute"
                          }}
                        >
                          <i class="fas fa-chevron-left"></i>
                          {" Back"}
                        </button>
                      </Link>
                    </li>
                    <li>
                      <input
                        name="setName"
                        placeholder="ชื่อ"
                        onChange={e => setName(e.target.value)}
                        value={name}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setCategory"
                        placeholder="ประเภท"
                        onChange={e => setCategorie(e.target.value)}
                        value={categorie}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setSize"
                        placeholder="ขนาด"
                        onChange={e => setSize(e.target.value)}
                        value={size}
                      />
                    </li>
                    <li>
                      <input
                        name="setOs"
                        placeholder="ระบบปฏิบัตการ"
                        onChange={e => setOs(e.target.value)}
                        value={os}
                      />
                    </li>
                    <li>
                      <input
                        name="setCopyright"
                        placeholder="บริษัท"
                        onChange={e => setCompany(e.target.value)}
                        value={company}
                      />
                    </li>

                    <li>
                      <input
                        name="setVersion"
                        placeholder="วันที่ออกจำหน่าย"
                        onChange={e => setDate_out(e.target.value)}
                        value={date_out}
                      />
                    </li>
                    <li>
                      <input
                        name="setLink"
                        placeholder="CPU"
                        onChange={e => setCpu(e.target.value)}
                        value={cpu}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setIcon"
                        placeholder="Ram"
                        onChange={e => setRam(e.target.value)}
                        value={ram}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setIcon"
                        placeholder="GPU"
                        onChange={e => setGrafic(e.target.value)}
                        value={grafic}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setIcon"
                        placeholder="HDD"
                        onChange={e => setDisk(e.target.value)}
                        value={disk}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setIcon"
                        placeholder="video"
                        onChange={e => setVideo(e.target.value)}
                        value={video}
                        required
                      />
                    </li>
                  </ul>
                </div>
                <div className="col col-md-7 c2">
                  <ul>
                    <li>
                      <input
                        name="setIcon"
                        placeholder="icon"
                        onChange={e => setIcon(e.target.value)}
                        value={icon}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setPic"
                        placeholder="รูปภาพ 1"
                        onChange={e => setPic(e.target.value)}
                        value={pic}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setPic2"
                        placeholder="รูปภาพ 2"
                        onChange={e => setPic2(e.target.value)}
                        value={pic2}
                      />
                    </li>
                    <li>
                      <input
                        name="setPic2"
                        placeholder="รูปภาพ 3"
                        onChange={e => setPic3(e.target.value)}
                        value={pic3}
                      />
                    </li>
                    <li>
                      <input
                        name="setPic2"
                        placeholder="รูปภาพ 4"
                        onChange={e => setPic4(e.target.value)}
                        value={pic4}
                      />
                    </li>
                    <li>
                      <textarea
                        className="text"
                        name="setText"
                        placeholder="รายละเอียด"
                        onChange={e => setInfo(e.target.value)}
                        value={info}
                        required
                      />
                    </li>
                    <li>
                      <button
                        className="btn btn-lg btn-success btnAdd"
                        type="submit"
                      >
                        <i class="fas fa-plus"></i>
                        {" Add Game"}
                      </button>
                    </li>
                  </ul>
                </div>
              </div>
            </form>
          </div>
        </section>
      </Fragment>
    </div>
  );
};

AdminGameAdd.propTypes = {
  postGame: PropTypes.func.isRequired
};

export default connect(null, { postGame })(AdminGameAdd);
