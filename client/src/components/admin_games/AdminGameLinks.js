import React from "react";
import PropTypes from "prop-types";
import { Fragment, useState, useEffect } from "react";
import "../../css/admin.css";
import { Link } from "react-router-dom";
import {
  postGameLink,
  getLinksGame,
  getGameById,
  deleteGameLink
} from "../../actions/game";
import { connect } from "react-redux";
import ItemLink from "./ItemLink";

const AdminGameLinks = ({
  postGameLink,
  getLinksGame,
  getGameById,
  deleteGameLink,
  game: { game, game_links, loading },
  match
}) => {
  const [link, setLink] = useState("");
  const [type, setType] = useState("");
  const [num, setNum] = useState("");

  useEffect(() => {
    window.scrollTo(0, 0);
    getLinksGame(match.params.id);
    getGameById(match.params.id);
  }, [getLinksGame, getGameById]);

  const onSubmit = e => {
    e.preventDefault();

    const data = {
      link: link,
      type: type,
      num: num
    };

    if (data.link.length === 0 || data.type.length === 0) {
      alert("กรุณกรอกลิ้งค์ และประเภทลิ้งค์");
    } else {
      postGameLink(match.params.id, data);
    }
  };

  return (
    <div>
      <Fragment>
        <section className="section-content">
          <div className="container-lg">
            <div className="row">
              <div className="col col-md-5 c1">
                <form onSubmit={onSubmit}>
                  <ul>
                    <li>
                      <Link to={"/admin/games"}>
                        <button
                          className="btn btn-sm btn-dark btnAdd"
                          style={{
                            marginLeft: "-80px",
                            marginTop: "-80px",
                            marginBottom: "10px",
                            position: "absolute"
                          }}
                        >
                          <i class="fas fa-chevron-left"></i>
                          {" Back"}
                        </button>
                      </Link>
                    </li>
                    <li>
                      <img
                        style={{ width: "150px", height: "180px" }}
                        src={game.icon}
                      />
                      <span>{game.name}</span>
                    </li>
                    <li>
                      <input
                        name="link"
                        placeholder="ลิ้งค์"
                        onChange={e => setLink(e.target.value)}
                        value={link}
                        required
                      />
                    </li>
                    <li>
                      <select
                        className="selcetType"
                        onChange={e => setType(e.target.value)}
                      >
                        <option value="0">เลือก</option>
                        <option value="1">ไฟล์เดียว</option>
                        <option value="2">แบ่งพาร์ท</option>
                        <option value="3">Crack</option>
                        <option value="4">Patch</option>
                      </select>
                    </li>
                    <li>
                      <input
                        name="num"
                        type="number"
                        placeholder="ลำดับ"
                        onChange={e => setNum(e.target.value)}
                        value={num}
                      />
                    </li>
                    <li>
                      <button
                        className="btn btn-lg btn-success btnAdd"
                        type="submit"
                      >
                        <i class="fas fa-plus"></i>
                        {" Add Link"}
                      </button>
                    </li>
                  </ul>
                </form>
              </div>
              <div className="col col-md-7 c2">
                <ul>
                  {loading ? (
                    ""
                  ) : (
                    <Fragment>
                      {game_links.map((link, index) => (
                        <ItemLink
                          key={index}
                          id_game={match.params.id}
                          id={link.id}
                          type={link.type}
                          num={link.num}
                          link={link.link}
                        />
                      ))}
                    </Fragment>
                  )}
                </ul>
              </div>
            </div>
          </div>
        </section>
      </Fragment>
    </div>
  );
};

AdminGameLinks.propTypes = {
  postGameLink: PropTypes.func.isRequired,
  getLinksGame: PropTypes.func.isRequired,
  getGameById: PropTypes.func.isRequired,
  deleteGameLink: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  game: state.game
});

export default connect(mapStateToProps, {
  postGameLink,
  getLinksGame,
  getGameById,
  deleteGameLink
})(AdminGameLinks);
