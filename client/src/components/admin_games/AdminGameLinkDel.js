import React, { useEffect } from "react";
import { deleteGameLink } from "../../actions/game";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import history from "../../history";

const AdminGameLinkDel = ({ deleteGameLink, match }) => {
  useEffect(() => {
    deleteGameLink(match.params.id);
  });

  return (
    <div>
      {
        (history.push(`/admin/game/links/${match.params.id_game}`),
        window.location.reload())
      }
    </div>
  );
};

AdminGameLinkDel.propTypes = {
  deleteGameLink: PropTypes.func.isRequired
};

export default connect(null, { deleteGameLink })(AdminGameLinkDel);
