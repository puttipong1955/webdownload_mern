import React from "react";
import PropTypes from "prop-types";
import { Fragment, useState, useEffect } from "react";
import "../../css/admin.css";
import { Link, Redirect } from "react-router-dom";
import { putGame, getGameById } from "../../actions/game";
import { connect } from "react-redux";

const AdminGameEdit = ({ putGame, getGameById, game: { game }, match }) => {
  const [name, setName] = useState("name");
  const [categorie, setCategorie] = useState("categorie");
  const [size, setSize] = useState("size");
  const [os, setOs] = useState("os");
  const [company, setCompany] = useState("company");
  const [date_out, setDate_out] = useState("date_out");
  const [cpu, setCpu] = useState("cpu");
  const [ram, setRam] = useState("ram");
  const [grafic, setGrafic] = useState("grafic");
  const [disk, setDisk] = useState("disk");
  const [video, setVideo] = useState("video");
  const [icon, setIcon] = useState("icon");
  const [pic, setPic] = useState("pic");
  const [pic2, setPic2] = useState("pic2");
  const [info, setInfo] = useState("info");
  const [pic3, setPic3] = useState("pic3");
  const [pic4, setPic4] = useState("pic4");

  useEffect(() => {
    getGameById(match.params.id);
  }, []);

  const explode = function() {
    if (name === "name") setName(game.name);
    if (categorie === "categorie") setCategorie(game.categorie);
    if (size === "size") setSize(game.size);
    if (os === "os") setOs(game.os);
    if (company === "company") setCompany(game.company);
    if (date_out === "date_out") setDate_out(game.date_out);
    if (cpu === "cpu") setCpu(game.cpu);
    if (ram === "ram") setRam(game.ram);
    if (grafic === "grafic") setGrafic(game.grafic);
    if (disk === "disk") setDisk(game.disk);
    if (video === "video") setVideo(game.video);
    if (icon === "icon") setIcon(game.icon);
    if (pic === "pic") setPic(game.pic);
    if (pic2 === "pic2") setPic2(game.pic2);
    if (pic3 === "pic3") setPic3(game.pic3);
    if (pic4 === "pic4") setPic4(game.pic4);
    if (info === "info") setInfo(game.info);
  };
  setTimeout(explode, 2000);

  const onSubmit = e => {
    e.preventDefault();

    const data = {
      name: name,
      categorie: categorie,
      info: info,
      company: company,
      date_out: date_out,
      size: size,
      os: os,
      cpu: cpu,
      ram: ram,
      grafic: grafic,
      disk: disk,
      video: video,
      icon: icon,
      pic: pic,
      pic2: pic2,
      pic3: pic3,
      pic4: pic4
    };

    if (
      data.name.length === 0 ||
      data.categorie.length === 0 ||
      data.info.length === 0 ||
      data.video.length === 0 ||
      data.icon.length === 0
    ) {
      alert("กรุณกรอกชื่อ, ประเภท, รายละเอียด, วิดีโอ, ไอคอนเกมส์");
    } else {
      putGame(match.params.id, data);
    }
  };

  return (
    <div>
      <Fragment>
        <section className="section-content">
          <div className="container-lg">
            <form noValidate onSubmit={onSubmit}>
              <div className="row">
                <div className="col col-md-5 c1">
                  <ul>
                    <li>
                      <Link to={"/admin/games"}>
                        <button
                          className="btn btn-sm btn-dark btnAdd"
                          style={{
                            marginLeft: "-80px",
                            marginTop: "-80px",
                            marginBottom: "10px",
                            position: "absolute"
                          }}
                        >
                          <i class="fas fa-chevron-left"></i>
                          {" Back"}
                        </button>
                      </Link>
                    </li>
                    <li>
                      <input
                        name="setName"
                        placeholder="ชื่อ"
                        onChange={e => setName(e.target.value)}
                        value={name}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setCategory"
                        placeholder="ประเภท"
                        onChange={e => setCategorie(e.target.value)}
                        value={categorie}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setSize"
                        placeholder="ขนาด"
                        onChange={e => setSize(e.target.value)}
                        value={size}
                      />
                    </li>
                    <li>
                      <input
                        name="setOs"
                        placeholder="ระบบปฏิบัตการ"
                        onChange={e => setOs(e.target.value)}
                        value={os}
                      />
                    </li>
                    <li>
                      <input
                        name="setCopyright"
                        placeholder="บริษัท"
                        onChange={e => setCompany(e.target.value)}
                        value={company}
                      />
                    </li>

                    <li>
                      <input
                        name="setVersion"
                        placeholder="วันที่ออกจำหน่าย"
                        onChange={e => setDate_out(e.target.value)}
                        value={date_out}
                      />
                    </li>
                    <li>
                      <input
                        name="setLink"
                        placeholder="CPU"
                        onChange={e => setCpu(e.target.value)}
                        value={cpu}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setIcon"
                        placeholder="Ram"
                        onChange={e => setRam(e.target.value)}
                        value={ram}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setIcon"
                        placeholder="GPU"
                        onChange={e => setGrafic(e.target.value)}
                        value={grafic}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setIcon"
                        placeholder="HDD"
                        onChange={e => setDisk(e.target.value)}
                        value={disk}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setIcon"
                        placeholder="video"
                        onChange={e => setVideo(e.target.value)}
                        value={video}
                        required
                      />
                    </li>
                  </ul>
                </div>
                <div className="col col-md-7 c2">
                  <ul>
                    <li>
                      <input
                        name="setIcon"
                        placeholder="icon"
                        onChange={e => setIcon(e.target.value)}
                        value={icon}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setPic"
                        placeholder="รูปภาพ 1"
                        onChange={e => setPic(e.target.value)}
                        value={pic}
                        required
                      />
                    </li>
                    <li>
                      <input
                        name="setPic2"
                        placeholder="รูปภาพ 2"
                        onChange={e => setPic2(e.target.value)}
                        value={pic2}
                      />
                    </li>
                    <li>
                      <input
                        name="setPic2"
                        placeholder="รูปภาพ 3"
                        onChange={e => setPic3(e.target.value)}
                        value={pic3}
                      />
                    </li>
                    <li>
                      <input
                        name="setPic2"
                        placeholder="รูปภาพ 4"
                        onChange={e => setPic4(e.target.value)}
                        value={pic4}
                      />
                    </li>
                    <li>
                      <textarea
                        className="text"
                        name="setText"
                        placeholder="รายละเอียด"
                        onChange={e => setInfo(e.target.value)}
                        value={info}
                        required
                      />
                    </li>
                    <li>
                      <button
                        className="btn btn-lg btn-success btnAdd"
                        type="submit"
                      >
                        <i class="fas fa-plus"></i>
                        {" Edit Game"}
                      </button>
                    </li>
                  </ul>
                </div>
              </div>
            </form>
          </div>
        </section>
      </Fragment>
    </div>
  );
};

AdminGameEdit.propTypes = {
  putGame: PropTypes.func.isRequired,
  getGameById: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  game: state.game
});

export default connect(mapStateToProps, { putGame, getGameById })(
  AdminGameEdit
);
