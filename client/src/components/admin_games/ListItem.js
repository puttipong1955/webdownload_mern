import React, { Fragment } from "react";
import Moment from "moment";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { deleteGame, deleteGameLinks } from "../../actions/game";
import { connect } from "react-redux";

const ListItem = ({
  deleteGame,
  deleteGameLinks,
  id,
  category,
  name,
  icon,
  text,
  date,
  app,
  download
}) => {
  let dateCreate = Moment(date)
    .utc()
    .format("YYYY-MM-DD");
  const newText = text.replace(/[<b></b><u></u><i></u>]/g, "");

  const onDelete = () => {
    if (window.confirm("คุณต้องการลบหรือไม่?") === true) {
      deleteGame(id);
      deleteGameLinks(id);
      window.location.reload();
    }
  };

  return (
    <Fragment>
      <div className="row">
        <div className="col col-md-2">
          <div className="viewdownload">
            <span>{download}</span>
            <p>Views</p>
          </div>
        </div>
        <div className="col col-md-8">
          <Link to={`/admin/game/links/${id}`}>
            <button
              class="btn btn-outline-success btn-sm"
              style={{
                position: "absolute",
                marginLeft: "-40px",
                marginTop: "58px"
              }}
            >
              Links
            </button>
          </Link>
          <Link to={`/game/${id}`}>
            <button
              class="btn btn-outline-info btn-sm"
              style={{
                position: "absolute",
                marginLeft: "-40px",
                marginTop: "88px"
              }}
            >
              View
            </button>
          </Link>
          <Link
            to={`/admin/game/${id}`}
            className="program-content list-group-item list-group-item-action"
          >
            <div className="program-list-content">
              <div className="program-list-image">
                <img src={icon} />
              </div>
              <div
                className="program-list-text"
                style={{ marginLeft: "-50px" }}
              >
                <span>{name}</span>
                <p>{newText}</p>
                <i className="calendar fa fa-calendar-alt" aria-hidden="false">
                  <sub>{dateCreate}</sub>
                </i>
              </div>
            </div>
          </Link>
        </div>
        <div className="col col-md-2 btnDelete">
          <button className="btn btn-danger btn-sm" onClick={onDelete}>
            Delete
          </button>
        </div>
      </div>
    </Fragment>
  );
};

ListItem.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  deleteGame: PropTypes.func.isRequired,
  deleteGameLinks: PropTypes.func.isRequired
};

export default connect(null, { deleteGame, deleteGameLinks })(ListItem);
