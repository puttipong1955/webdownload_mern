import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { Link, Redirect } from "react-router-dom";

const CategoryItem = ({ label, now }) => {
  return (
    <Fragment>
      {label === now ? (
        <a className="cate-list-item choose list-group-item list-group-item-action list-group-item-light">
          {label}
        </a>
      ) : (
        <Link to={`/applications/category/${label}`}>
          <a
            onClick={<Redirect to={`/applications/category/${label}`} />}
            className="cate-list-item list-group-item list-group-item-action list-group-item-light"
          >
            {label}
          </a>
        </Link>
      )}
    </Fragment>
  );
};

CategoryItem.propTypes = {
  label: PropTypes.string.isRequired
};

export default CategoryItem;
