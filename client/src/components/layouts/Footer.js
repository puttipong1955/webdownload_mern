import React from "react";
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact";
import "../../css/footer.css";

const Footer = () => {
  return (
    <footer className="site-footer">
      <div className="container">
        <div className="row">
          <div className="col-sm-12 col-md-6">
            <h6>เกี่ยวกับเรา</h6>
            <p className="text-justify">
              Scanfcode.com <i>CODE WANTS TO BE SIMPLE </i> is an initiative to
              help the upcoming programmers with the code. Scanfcode focuses on
              providing the most efficient code or snippets as the code wants to
              be simple. We will help programmers build up concepts in different
              programming languages that include C, C++, Java, HTML, CSS,
              Bootstrap, JavaScript, PHP, Android, SQL and Algorithm.
            </p>
          </div>
          <div className="col-xs-6 col-md-3">
            <h6>หมวดหมู่</h6>
            <ul className="footer-links">
              <li>
                <a href="programs.html">โปรแกรม</a>
              </li>
              <li>
                <a href="applications.html">แอพพลิเคชั่น</a>
              </li>
              <li>
                <a href="games.html">เกมส์</a>
              </li>
            </ul>
          </div>
          <div className="col-xs-6 col-md-3">
            <h6>ติดต่อเรา</h6>
            <ul className="footer-links">
              <li>
                <i className="fas fa-mobile-alt" /> +66 955456916
              </li>
              <li>
                <i className="fas fa-at" /> pgmoney1955@gmail.com
              </li>
            </ul>
          </div>
        </div>
        <hr />
      </div>
      <div className="container">
        <div className="row">
          <div className="col-md-8 col-sm-6 col-xs-12">
            <p className="copyright-text">
              Copyright © 2020 All Rights Reserved by
              <a href="#">PG48soft</a>.
            </p>
          </div>
          <div className="col-md-4 col-sm-6 col-xs-12">
            <ul className="social-icons">
              <li>
                <a className="facebook" href="#">
                  <i className="fab fa-facebook-f" />
                </a>
              </li>
              <li>
                <a className="twitter" href="#">
                  <i className="fab fa-twitter" />
                </a>
              </li>
              <li>
                <a className="dribbble" href="#">
                  <i className="fab fa-dribbble" />
                </a>
              </li>
              <li>
                <a className="linkedin" href="#">
                  <i className="fab fa-linkedin" />
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
