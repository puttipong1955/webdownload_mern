import React, { useState, Fragment } from "react";
import Logo from "../../img/PAGload-logos_transparent.png";
import { Link } from "react-router-dom";
import "../../css/style-scollbar.css";
import "../../css/nav.css";
import history from "../../history";
import { connect } from "react-redux";
import { logout } from "../../actions/auth";
import PropTypes from "prop-types";

export const Nav = ({ auth: { isAuthenticated, loading }, logout }) => {
  const [text, setText] = useState("");

  const onSubmit = () => {
    {
      text.length === 0
        ? alert("!กรุณาใส่ข้อความที่ต้องการค้นหา")
        : history.push(`/programs/search/${text}`);
    }
  };

  const guestLinks = (
    <nav className="navbar navbar-dark py-0 bg-primary navbar-expand-lg py-md-0">
      <Link className="navbar-brand" to="/">
        <img src={Logo} alt="logo" style={{ width: "50px" }} />
      </Link>

      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#collapse_Navbar"
      >
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="collapse_Navbar">
        <ul className="navbar-nav">
          <li className="nav-item">
            <Link className="nav-link" to="/">
              <button className="btn btn-primary">หน้าแรก</button>
            </Link>
          </li>

          <li className="nav-item">
            <Link className="nav-link" to="/programs">
              <button className="btn btn-primary">โปรแกรม</button>
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/applications">
              <button className="btn btn-primary">แอพพลิเคชั่น</button>
            </Link>
          </li>

          <li className="nav-item">
            <Link className="nav-link" to="/games">
              <button className="btn btn-primary active">เกมส์</button>
            </Link>
          </li>
        </ul>
        <form className="form-inline form-phone" onSubmit={e => onSubmit(e)}>
          <input
            className="form-control mr-sm-2"
            type="text"
            value={text}
            onChange={e => setText(e.target.value)}
            placeholder="ค้นหาสิ่งที่ต้องการ..."
          />
          <button className="btn btn-primary" type="submit">
            ค้นหา
          </button>
        </form>
      </div>

      <form className="form-inline form-desktop" onSubmit={e => onSubmit(e)}>
        <input
          className="form-control mr-sm-2"
          type="text"
          value={text}
          onChange={e => setText(e.target.value)}
          placeholder="ค้นหาสิ่งที่ต้องการ..."
        />
        <button className="btn btn-primary" type="submit">
          ค้นหา
        </button>
      </form>
    </nav>
  );

  const authLinks = (
    <nav className="navbar navbar-dark py-0 bg-primary navbar-expand-lg py-md-0">
      <Link className="navbar-brand" to="/">
        <img src={Logo} alt="logo" style={{ width: "40px" }} />
      </Link>

      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#collapse_Navbar"
      >
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="collapse_Navbar">
        <ul className="navbar-nav">
          <li className="nav-item">
            <Link className="nav-link" to="/admin/dashboard">
              <button className="btn btn-primary">หน้าแรก</button>
            </Link>
          </li>

          <li className="nav-item">
            <Link className="nav-link" to="/admin/programs">
              <button className="btn btn-primary">โปรแกรม</button>
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/admin/applications">
              <button className="btn btn-primary">แอพพลิเคชั่น</button>
            </Link>
          </li>

          <li className="nav-item">
            <Link className="nav-link" to="/admin/games">
              <button className="btn btn-primary active">เกมส์</button>
            </Link>
          </li>
        </ul>
      </div>
      <button onClick={logout} className="btn btn-primary">
        ออกจากระบบ
      </button>
    </nav>
  );

  return (
    <div>{<Fragment>{isAuthenticated ? authLinks : guestLinks}</Fragment>}</div>
  );
};

Nav.propTypes = {
  logout: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { logout })(Nav);
