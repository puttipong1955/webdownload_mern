import React, { Fragment } from "react";
import loadbar from "./loading.gif";

export default () => (
  <Fragment>
    <img
      src={loadbar}
      style={{ width: "400px", height: "250px", margin: "auto", display: "block", marginLeft: "-75px" }}
      alt="Loading..."
    />
  </Fragment>
);
