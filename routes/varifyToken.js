// Fomat of Token
// Authentication: bearer <access_token>
// VarifyToken
function varifyToken(req, res, next) {
  // Get auth headers value
  const BearerHeader = req.headers["authorization"];
  // Check if bearer is underfind
  if (typeof BearerHeader !== "undefined") {
    // Spilt at the space
    const bearer = BearerHeader.split(" ");
    // Get token from array
    const bearerToken = bearer[1];
    // Set the token
    req.token = bearerToken;
    // Next middleware
    next();
  } else {
    // Forbidden
    res.sendStatus(403);
  }
}

module.exports = varifyToken;
