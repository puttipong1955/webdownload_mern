var express = require("express");
var Router = express.Router();
var jwt = require("jsonwebtoken");
const passport = require("passport");
const auth = require("../middleware/auth");

const db = require("../utils/db.config");
const User = db.user;

// @route    GET api/auth
// @decs     Test route
// @access   Public
Router.get("/", auth, async (req, res) => {
  try {
    const user = await User.findByPk(req.user.id);
    res.json(user);
  } catch (err) {
    console.log(err.message);
    res.status(500).send("Sever Error");
  }
  res.json("Auth...");
});

// Login
Router.post("/", (req, res) => {
  const { email, password } = req.body;

  User.findOne({ where: { email: email } }).then(user => {
    // Check for user
    if (!user) {
      console.log("User Not Fould");
      res.json("User Not Fould");
    } else if (user.userPassword === password) {
      const payload = {
        id: user.userId,
        email: user.userEmail,
        username: user.userUsername
      }; // Create JWT Payload

      // Sign Token
      jwt.sign(payload, "secretkey", { expiresIn: 3600 }, (err, token) => {
        res.json({
          success: true,
          token: "Bearer " + token
        });
      });
    } else {
      console.log("Password wrong!");
      res.json("Password wrong!");
    }
  });
});

// @route   GET api/users/current
// @desc    Return current user
// @access  Private
Router.get(
  "/current",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    res.json({
      id: req.user.userId,
      email: req.user.userEmail
    });
  }
);

module.exports = Router;
