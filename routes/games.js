var express = require("express");
var Router = express.Router();
var dbConn = require("../utils/Connection");

// Get games last limit 5
Router.get("/last", (req, res) => {
  dbConn.query(
    "SELECT * FROM `tbl_games` ORDER BY `tbl_games`.`date` DESC LIMIT 5",
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

// Get games last limit 15
Router.get("/limit", (req, res) => {
  dbConn.query(
    "SELECT * FROM `tbl_games` ORDER BY `tbl_games`.`date` DESC LIMIT 24",
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

// Get games last
Router.get("/", (req, res) => {
  dbConn.query(
    "SELECT * FROM `tbl_games` ORDER BY `tbl_games`.`date` DESC",
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

// Get games most limit 16
Router.get("/most", (req, res) => {
  dbConn.query(
    "SELECT * FROM `tbl_games` ORDER BY `tbl_games`.`views` DESC LIMIT 16",
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

// Get game by id
Router.get("/:id", (req, res) => {
  dbConn.query(
    "SELECT * FROM tbl_games WHERE id = " + dbConn.escape(req.params.id),
    (err, rows, fields) => {
      if (!err) {
        res.send(rows[0]);
      } else {
        console.log(err);
      }
    }
  );
});

// Get game by category
Router.get("/category/:category", (req, res) => {
  dbConn.query(
    "SELECT * FROM `tbl_games` WHERE `categorie` = " +
      dbConn.escape(req.params.category) +
      " ORDER BY `tbl_games`.`date` DESC",
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

// Get links game
Router.get("/links/:id", (req, res) => {
  dbConn.query(
    "SELECT * FROM `tbl_games_links` WHERE `tbl_games_id` = " +
      dbConn.escape(req.params.id),
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

//update game downloaded
Router.put("/download/:id", (req, res) => {
  let sql =
    "UPDATE `tbl_games` SET `views` = `views` + 1 WHERE `id` = " +
    req.params.id +
    "";
  dbConn.query(sql, (err, results) => {
    if (!err) {
      res.json(results);
      console.log(results);
    } else {
      console.log(err);
    }
  });
});

//Delete game
Router.delete("/:id", (req, res) => {
  let sql = "DELETE FROM tbl_games WHERE id=" + req.params.id + "";
  dbConn.query(sql, (err, results) => {
    if (!err) {
      res.json(results);
      console.log(results);
    } else {
      console.log(err);
    }
  });
});

// Add game
Router.post("/", (req, res) => {
  let form = req.body;
  let data = {
    name: form.name,
    categorie: form.categorie,
    info: form.info,
    company: form.company,
    date_out: form.date_out,
    size: form.size,
    os: form.os,
    cpu: form.cpu,
    ram: form.ram,
    grafic: form.grafic,
    disk: form.disk,
    video: form.video,
    icon: form.icon,
    pic: form.pic,
    pic2: form.pic2,
    pic3: form.pic3,
    pic4: form.pic4
  };
  let sql = "INSERT INTO tbl_games SET ?";

  dbConn.query(sql, data, (err, rows, fields) => {
    if (!err) {
      res.json(rows);
      console.log(rows);
    } else {
      console.log(err);
    }
  });
});

// Update game
Router.put("/:id", (req, res) => {
  const id = req.params.id;
  let form = req.body;

  let sql =
    "UPDATE tbl_games SET name='" +
    form.name +
    "', categorie='" +
    form.categorie +
    "', info='" +
    form.info +
    "', company='" +
    form.company +
    "', date_out='" +
    form.date_out +
    "', size='" +
    form.size +
    "', os='" +
    form.os +
    "', cpu='" +
    form.cpu +
    "', ram='" +
    form.ram +
    "', grafic='" +
    form.grafic +
    "', disk='" +
    form.disk +
    "', video='" +
    form.video +
    "', icon='" +
    form.icon +
    "', pic='" +
    form.pic +
    "', pic2='" +
    form.pic2 +
    "', pic3='" +
    form.pic3 +
    "', pic4='" +
    form.pic4 +
    "' WHERE id=" +
    id;

  dbConn.query(sql, (err, rows, data) => {
    if (!err) {
      res.json(data);
      console.log(rows);
    } else {
      console.log(err);
    }
  });
});

// Add link game
Router.post("/link/:id", (req, res) => {
  let form = req.body;
  let data = {
    tbl_games_id: req.params.id,
    link: form.link,
    type: form.type,
    num: form.num
  };
  let sql = "INSERT INTO tbl_games_links SET ?";

  dbConn.query(sql, data, (err, rows, fields) => {
    if (!err) {
      res.json(rows);
      console.log(rows);
    } else {
      console.log(err);
    }
  });
});

// Delete game link
Router.delete("/links/:id", (req, res) => {
  let sql =
    "DELETE FROM tbl_games_links WHERE tbl_games_id=" + req.params.id + "";
  dbConn.query(sql, (err, results) => {
    if (!err) {
      res.json(results);
      console.log(results);
    } else {
      console.log(err);
    }
  });
});

// Delete game link
Router.delete("/link/:id", (req, res) => {
  let sql = "DELETE FROM tbl_games_links WHERE id=" + req.params.id + "";
  dbConn.query(sql, (err, results) => {
    if (!err) {
      res.json(results);
      console.log(results);
    } else {
      console.log(err);
    }
  });
});

module.exports = Router;
