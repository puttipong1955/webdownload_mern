var express = require("express");
var Router = express.Router();
var dbConn = require("../utils/Connection");

var varifyToken = require("./varifyToken");
var jwt = require("jsonwebtoken");

Router.post("/test", varifyToken, (req, res) => {
  jwt.verify(req.token, "secretkey", (err, authData) => {
    if (err) {
      res.sendStatus(403);
    } else {
      res.json({ message: "testting...", authData });
    }
  });
});

// Get programs random limit 10
Router.get("/random", (req, res) => {
  dbConn.query(
    "SELECT * FROM tbl_programs WHERE application = '0' ORDER BY RAND() LIMIT 10",
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

// Get programs populars limit 5
Router.get("/populars", (req, res) => {
  dbConn.query(
    "SELECT * FROM `tbl_programs` WHERE application = '0' ORDER BY `tbl_programs`.`download` DESC LIMIT 5",
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

// Get all programs new
Router.get("/", (req, res) => {
  dbConn.query(
    "SELECT * FROM `tbl_programs` WHERE application = '0' ORDER BY `tbl_programs`.`create_date` DESC",
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

// Get all programs new limit
Router.get("/limit", (req, res) => {
  dbConn.query(
    "SELECT * FROM `tbl_programs` WHERE application = '0' ORDER BY `tbl_programs`.`create_date` DESC LIMIT 7",
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

// Get program by id
Router.get("/:id", (req, res) => {
  dbConn.query(
    "SELECT * FROM tbl_programs WHERE application = '0' AND id = " +
      dbConn.escape(req.params.id),
    (err, row, fields) => {
      if (!err) {
        res.send(row[0]);
      } else {
        console.log(err);
      }
    }
  );
});

// Get categories program
Router.get("/category/group", (req, res) => {
  dbConn.query(
    "SELECT category FROM `tbl_programs` WHERE application = '0' GROUP BY category",
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

// Get programs by categor lated
Router.get("/category/:category", (req, res) => {
  dbConn.query(
    "SELECT * FROM tbl_programs WHERE application = '0' AND category = " +
      dbConn.escape(req.params.category) +
      "ORDER BY tbl_programs.create_date DESC",
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

// Get programs by category random
Router.get("/category/:category/random", (req, res) => {
  dbConn.query(
    "SELECT * FROM tbl_programs WHERE application = '0' AND category = " +
      dbConn.escape(req.params.category) +
      "ORDER BY RAND() LIMIT 3",
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

// Add program or application
Router.post("/", (req, res) => {
  let form = req.body;
  let data = {
    name: form.name,
    category: form.category,
    text: form.text,
    size: form.size,
    os: form.os,
    copyright: form.copyright,
    version: form.version,
    link: form.link,
    download: form.download,
    application: form.application,
    icon: form.icon,
    pic: form.pic,
    pic2: form.pic2,
    news: form.news
  };
  let sql = "INSERT INTO tbl_programs SET ?";

  dbConn.query(sql, data, (err, rows, fields) => {
    if (!err) {
      res.json(rows);
      console.log(rows);
    } else {
      console.log(err);
    }
  });
});

// Update program or application
Router.put("/:id", (req, res) => {
  const id = req.params.id;
  let form = req.body;

  let sql =
    "UPDATE tbl_programs SET name='" +
    form.name +
    "', category='" +
    form.category +
    "', text='" +
    form.text +
    "', size='" +
    form.size +
    "', os='" +
    form.os +
    "', copyright='" +
    form.copyright +
    "', version='" +
    form.version +
    "', link='" +
    form.link +
    "', news='" +
    form.news +
    "', application='" +
    form.application +
    "', icon='" +
    form.icon +
    "', pic='" +
    form.pic +
    "', pic2='" +
    form.pic2 +
    "' WHERE id=" +
    id;

  dbConn.query(sql, (err, rows, data) => {
    if (!err) {
      res.json(data);
      console.log(rows);
    } else {
      console.log(err);
    }
  });
});

//Delete program or application
Router.delete("/:id", (req, res) => {
  let sql = "DELETE FROM tbl_programs WHERE id=" + req.params.id + "";
  dbConn.query(sql, (err, results) => {
    if (!err) {
      res.json(results);
      console.log(results);
    } else {
      console.log(err);
    }
  });
});

//Search program or application or game
Router.get("/search/:key", (req, res) => {
  let sql =
    "select `id`, `name`, `text`, `category`, `icon`, `application`, `create_date` from `tbl_programs` where lower(`name`) like '%" +
    req.params.key +
    "%' OR lower(`category`) like '%" +
    req.params.key +
    "%' OR UPPER(`name`) like UPPER('%" +
    req.params.key +
    "%') OR UPPER(`category`) like UPPER('%" +
    req.params.key +
    "%') UNION ALL select `id`, `name`, `info`, `categorie`, `icon`, `application`, `date` from `tbl_games` where lower(`name`) like '%" +
    req.params.key +
    "%' OR lower(`categorie`) like '%" +
    req.params.key +
    "%' OR UPPER(`name`) like ('%" +
    req.params.key +
    "%') OR UPPER(`categorie`) like UPPER('%" +
    req.params.key +
    "%')";
  dbConn.query(sql, (err, results) => {
    if (!err) {
      res.json(results);
      console.log(results);
    } else {
      console.log(err);
    }
  });
});

//update program download
Router.put("/download/:id", (req, res) => {
  let sql =
    "UPDATE `tbl_programs` SET `download` = `download` + 1 WHERE `id` = " +
    req.params.id +
    "";
  dbConn.query(sql, (err, results) => {
    if (!err) {
      res.json(results);
      console.log(results);
    } else {
      console.log(err);
    }
  });
});

//Search program or application or game
Router.get("/tag/:key", (req, res) => {
  let sql =
    "select `id`, `name`, `tag`, `category`, `text`, `application`, `icon`, `create_date` from `tbl_programs` where lower(`tag`) like '%" +
    req.params.key +
    "%' UNION ALL select `id`, `name`, `tag`, `categorie`, `info`,  `application`, `icon`, `date` from `tbl_games` where lower(`tag`) like '%" +
    req.params.key +
    "%'";
  dbConn.query(sql, (err, results) => {
    if (!err) {
      res.json(results);
      console.log(results);
    } else {
      console.log(err);
    }
  });
});

module.exports = Router;
