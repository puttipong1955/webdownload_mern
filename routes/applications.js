var express = require("express");
var Router = express.Router();
var dbConn = require("../utils/Connection");

// Get applications random limit 10
Router.get("/random", (req, res) => {
  dbConn.query(
    "SELECT * FROM tbl_programs WHERE application = '1' ORDER BY RAND() LIMIT 10",
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

// Get applications populars limit 5
Router.get("/populars", (req, res) => {
  dbConn.query(
    "SELECT * FROM `tbl_programs` WHERE application = '1' ORDER BY `tbl_programs`.`download` DESC LIMIT 5",
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

// Get all applications
Router.get("/", (req, res) => {
  dbConn.query(
    "SELECT * FROM tbl_programs WHERE application = '1' ORDER BY `tbl_programs`.`create_date` DESC",
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

// Get all programs new limit
Router.get("/limit", (req, res) => {
  dbConn.query(
    "SELECT * FROM `tbl_programs` WHERE application = '1' ORDER BY `tbl_programs`.`create_date` DESC LIMIT 7",
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

// Get applications by id
Router.get("/:id", (req, res) => {
  dbConn.query(
    "SELECT * FROM tbl_programs WHERE application = '1' AND id = " +
      dbConn.escape(req.params.id),
    (err, rows, fields) => {
      if (!err) {
        res.send(rows[0]);
      } else {
        console.log(err);
      }
    }
  );
});

// Get categories application
Router.get("/category/group", (req, res) => {
  dbConn.query(
    "SELECT category FROM `tbl_programs` WHERE application = '1' GROUP BY category",
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

// Get applications by category lated
Router.get("/category/:category", (req, res) => {
  dbConn.query(
    "SELECT * FROM tbl_programs WHERE application = '1' AND category = " +
      dbConn.escape(req.params.category) +
      "ORDER BY tbl_programs.create_date DESC",
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

// Get applications by category random
Router.get("/category/:category/random", (req, res) => {
  dbConn.query(
    "SELECT * FROM tbl_programs WHERE application = '1' AND category = " +
      dbConn.escape(req.params.category) +
      "ORDER BY RAND() LIMIT 3",
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
      } else {
        console.log(err);
      }
    }
  );
});

module.exports = Router;
