var mysql = require("mysql");

var dbConn = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "pg48solf",
  multipleStatements: true
});

dbConn.connect(err => {
  if (!err) {
    console.log("Connected");
  } else {
    console.log("Connecttion Faild");
  }
});

module.exports = dbConn;
