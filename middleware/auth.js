const jwt = require("jsonwebtoken");
const config = require("config");

module.exports = function(req, res, next) {
  // Get token from header
  const token = req.header("x-auth-token");
  const tokenConvert = token.substring(7);

  // Check token
  if (!token) {
    return res.status(401).json({
      msg: "No token, authorization denied"
    });
  }

  // Verify token
  try {
    const decoded = jwt.verify(tokenConvert, config.get("jwtSecret"));

    req.user = decoded;
    next();
  } catch (err) {
    res.status(401).json({
      msg: "Token is not valid"
    });
  }
};
