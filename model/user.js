module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define(
    "tbl_users",
    {
      userId: {
        type: Sequelize.INTEGER,
        field: "id",
        primaryKey: true
      },
      userEmail: {
        type: Sequelize.STRING,
        field: "email"
      },
      userPassword: {
        type: Sequelize.STRING,
        field: "password"
      },
      userUsername: {
        type: Sequelize.STRING,
        field: "username"
      }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  );
  return User;
};
