var express = require("express");
var app = express();
var bodyParser = require("body-parser");
const passport = require("passport");

//Router
const ProgramsRoutes = require("./routes/programs");
const ApplicationsRoutes = require("./routes/applications");
const AuthenticationRoutes = require("./routes/authentication");
const GameRoutes = require("./routes/games");

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

// Passport middleware
app.use(passport.initialize());

// Passport Config
require("./config/passport")(passport);

app.use("/api/programs", ProgramsRoutes);
app.use("/api/applications", ApplicationsRoutes);
app.use("/api/auth", AuthenticationRoutes);
app.use("/api/games", GameRoutes);
// default route
app.get("/", function(req, res) {
  return res.send({ error: true, message: "hello" });
});

// connection configurations

app.listen(5000);
